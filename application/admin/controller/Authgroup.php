<?php
namespace app\admin\controller;

use app\common\model\AuthGroup as AuthGroupModel;
use app\common\model\AuthRule;
use think\facade\Url;
use szktor\Tree;

class Authgroup extends BaseAdmin
{
	  protected $noNeedLogin = [];
    protected $noNeedRight = [];

    //初始化
    public function initialize()
    {
      parent::initialize();
      $this->assign('menu_tag','menu_one');  //菜单组标记，与系统菜单节点相对应
    }

    public function index()
    {
        if($this->request->isAjax())
        {
          $page = intval($this->request->post('page'));
          $vstatus = trim($this->request->post('vstatus'));
          $word = trim($this->request->post('word'));
          //分页
          $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
          $offset = intval(($page - 1) * intval($limit));  //开始位置
          //条件
          $_map = [];
          //类型
          if(strtoupper($vstatus) != 'ALL')
          {
             $_map[] = ['status', 'EQ', intval($vstatus)];
          }
          if($word)
          {
             $_map[] = ['name|remark','LIKE', '%' . $word . '%'];
          }
          $arrList = AuthGroupModel::where($_map)->order('id desc')->limit($offset, $limit)->select();
          $arrCount = AuthGroupModel::where($_map)->count();
          if($arrCount <= 0 || count($arrList) <= 0)
          {
             return json(['err' => '1', 'msg' => '当前页下的数据已显示完毕！']);
          }
          foreach($arrList as $k => $v)
          {
              $arrList[$k]['status'] = intval($v['status']) > 0 ? '启用' : '禁用';
          }
          //树形
          if(strtoupper($vstatus) == 'ALL' && !$word && count($arrList) > 1)
          {
            $ruleList = $arrList->toArray();
            $tree = Tree::instance();
            $tree->init($ruleList);
            $arrList = $tree->getTreeList($tree->getTreeArray(0), 'name');
          }

          return json(['err' => '0', 'page' => $page , 'limit' => $limit, 'total' => $arrCount,'list' => $arrList]);
        }else{
          $page = intval($this->request->get('p')); //url 页码参数  ?p=1
          $this->assign('page', $page > 0 ? $page : '1');
          return $this->view->fetch();
        }
    }

    /*
      增加
    */
    public function add()
    {
        if($this->request->isAjax())
        {
            $v_title = trim($this->request->post('v_title'));
            $v_pid = intval($this->request->post('v_pid'));
            $v_status = intval($this->request->post('v_status'));
            $v_remark = trim($this->request->post('v_remark'));
            $v_admin = intval($this->request->post('v_admin'));
            $v_pages = trim($this->request->post('v_pages'));
            $v_arr = $this->request->post('v_arr/a');
            if(!$v_title)
            {
              return json(['err' => '1', 'msg' => '角色名称不可为空']);
            }
            $v_arr = !$v_arr ? '' : implode(',', $v_arr);
            if($v_admin > 0)
            {
               $v_arr = '*'; //星号表示所有权限
            }else{
               if(!$v_arr)
               {
                 return json(['err' => '2', 'msg' => '请设置角色组权限']);
               }
            }
            $iData = [
                'pid' => $v_pid, //父组别
                'name' => $v_title, //组名
                'rules' => $v_arr, //规则ID
                'create_time' => time(), //创建时间
                'update_time' => '0', //更新时间
                'status' => $v_status, //状态，1可用0禁用
                'remark' => $v_remark, //角色组操作信息备注
                'index_page' => $v_pages, //后台工作首页
            ];
            $newId = AuthGroupModel::insertGetId($iData);
            return json(['err' => '0', 'msg' => '系统角色添加成功']);
        }else{
           $this->assign('group',AuthGroupModel::getGroupList());
           $this->assign('rule',AuthRule::getGroupSetRuleList('add','','admin'));
           return $this->view->fetch();
        }
    }
    /*
      修改
    */
    public function edit()
    {
        if($this->request->isAjax())
        {
            $v_id = intval($this->request->post('v_id'));
            $v_title = trim($this->request->post('v_title'));
            $v_pid = intval($this->request->post('v_pid'));
            $v_status = intval($this->request->post('v_status'));
            $v_remark = trim($this->request->post('v_remark'));
            $v_admin = intval($this->request->post('v_admin'));
            $v_pages = trim($this->request->post('v_pages'));
            $v_arr = $this->request->post('v_arr/a');
            if(!$v_title)
            {
              return json(['err' => '1', 'msg' => '角色名称不可为空']);
            }
            $v_arr = !$v_arr ? '' : implode(',', $v_arr);
            if($v_admin > 0)
            {
               $v_arr = '*'; //星号表示所有权限
            }else{
               if(!$v_arr)
               {
                 return json(['err' => '2', 'msg' => '请设置角色组权限']);
               }
            }
            $iData = [
                'pid' => $v_pid, //父组别
                'name' => $v_title, //组名
                'rules' => $v_arr, //规则ID
                'update_time' => time(), //更新时间
                'status' => $v_status, //状态，1可用0禁用
                'remark' => $v_remark, //角色组操作信息备注
                'index_page' => $v_pages, //后台工作首页
            ];
            AuthGroupModel::where(['id' => $v_id])->update($iData);
            return json(['err' => '0', 'msg' => '系统角色信息修改成功']);
        }else{
           $id = intval($this->request->get('id'));
           $info = AuthGroupModel::where(['id' => $id])->order('id desc')->find();
           if(!$info)
           {
             $this->redirect('admin/authgroup/index', '', 302);
           }
           $this->assign('info',$info);
           $this->assign('group',AuthGroupModel::getGroupList());
           $this->assign('rule',AuthRule::getGroupSetRuleList('edit',$info,'admin'));
           return $this->view->fetch();
        }
    }
    /*
      删除
    */
    public function del()
    {
        if($this->request->isAjax())
        {
            $id = intval($this->request->post('id'));
            $res = AuthGroupModel::where(['id' => $id])->delete();
            if($res)
            {
              return json(['err' => '0' , 'msg' => '相关数据已删除成功！']);
            }else{
              return json(['err' => '1' , 'msg' => '已删除' . $res . '条相关数据！']);
            }
        }
        $this->redirect('admin/authgroup/index', '', 302);
    }

}

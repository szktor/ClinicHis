<?php
namespace app\admin\controller;

use think\facade\Url;
use think\facade\Cache;
use app\common\model\Config as CfgModel;
use szktor\SystemDict; //系统公共字典

class System extends BaseAdmin
{
	protected $noNeedLogin = [];
    protected $noNeedRight = [];

    //初始化
    public function initialize()
    {
      parent::initialize();
      $this->assign('menu_tag','menu_one'); //菜单组标记，与系统菜单节点相对应
    }

    public function index()
    {
        $arr = SystemDict::loadSystemConfigCate();
        //自定义的变量
        $custom = CfgModel::getWebConfigArray('custom');
        $add_custom = '1';
        if($custom && is_array($custom))
        {
          $arr[count($arr)] = [
              'title' => '自定义配置', 'group' => 'custom',
              'list' => $custom,
          ];
          $add_custom = '0';
        }
       $this->assign('add_custom', $add_custom);
       $this->assign('cfgarr',$arr);
       return $this->view->fetch();
    }
    
    //保存配置参数
    public function saveconfig()
    {
        if($this->request->isAjax())
        {
            $data = $this->request->post('data/a');
            if(!$data || !is_array($data))
            {
              return json(['err' => '1', 'msg' => '当前页面配置参数无需保存']);
            }
            $iData = '';
            foreach($data as $k => $v)
            {
              $iData[] = [
                  'id' => intval($v['id']),
                  'name' => trim($v['name']),
                  'value' => trim($v['value']),
                  'sort' => intval($v['sort']),
              ];
            }
            $model = new CfgModel;
            $model->saveAll($iData,true);
            return json(['err' => '0', 'msg' => '网站全局配置参数更新成功']);
        }
        $this->redirect('admin/system/index', '', 302);
    }
    
    //增加一个配置参数
    public function add()
    {
        if($this->request->isAjax())
        {
          $av_group = trim($this->request->post('av_group'));
          $av_title = trim($this->request->post('av_title'));
          $av_desc = trim($this->request->post('av_desc'));
          $av_name = trim($this->request->post('av_name'));
          $av_value = trim($this->request->post('av_value'));
          $av_type = trim($this->request->post('av_type'));
          $av_content = trim($this->request->post('av_content'));
          $av_sort = intval($this->request->post('av_sort'));
          if(!$av_group)
          {
             return json(['err' => '1', 'msg' => '请输入参数分组英文字母']); 
          }
          if(!$av_title)
          {
             return json(['err' => '2', 'msg' => '请输入变量标题']); 
          }
          if(!$av_name)
          {
             return json(['err' => '3', 'msg' => '请输入不可重复的变量名']); 
          }
          //重复判断
          $_count = CfgModel::where(['name' => $av_name])->order('id desc')->count();
          if($_count > 0)
          {
             return json(['err' => '4', 'msg' => '变量名【' . $av_name . '】系统已存在，禁止重复']);
          }
          $iData = [
               'name' => $av_name, //变量名
               'group' => $av_group, //分组
               'title' => $av_title, //变量标题
               'tip' => $av_desc, //变量描述
               'type' => $av_type, //类型:text,radio,select
               'value' => $av_value, //变量值
               'content' => $av_content, //变量字典数据格式：变量值文字名|值#变量值文字名|值，如开启|1#关闭|0
               'sort' => $av_sort, //排序值越小越靠前显示
          ];
          $newId = CfgModel::insertGetId($iData);
          return json(['err' => '0', 'msg' => '全局变量参数添加成功']);
        }
        $this->redirect('admin/system/index', '', 302);
   }

}

<?php
namespace app\admin\controller;

use app\common\model\Admin as AdminModel;
use app\common\model\AuthGroup;
use app\common\model\Shops as ShopsModel;
use app\common\model\StoreWarehouse as StoreWarehouseModel;

class Admin extends BaseAdmin
{
	  protected $noNeedLogin = [];
    protected $noNeedRight = [];
    
    //初始化
    public function initialize()
    {
      parent::initialize();
      $this->assign('menu_tag','menu_one');  //菜单组标记，与系统菜单节点相对应
    }

    public function index()
    {
        if($this->request->isAjax())
        {
          $page = intval($this->request->post('page'));
          $vstatus = trim($this->request->post('vstatus'));
          $vgroup = trim($this->request->post('vgroup'));
          $houseid = trim($this->request->post('houseid'));
          $word = trim($this->request->post('word'));
          $shopid = trim($this->request->post('shopid'));
          //分页
          $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
          $offset = intval(($page - 1) * intval($limit));  //开始位置
          //条件
          $_map = [['is_del','=','0']];
          //类型
          if(strtoupper($vstatus) != 'ALL')
          {
             $_map[] = ['status','EQ',intval($vstatus)];
          }
          //角色
          if(strtoupper($vgroup) != 'ALL')
          {
             $_map[] = ['group_id','EQ',intval($vgroup)];
          }
          //门店
          if(strtoupper($shopid) != 'ALL')
          {
             $_map[] = ['shop_id','EQ',intval($shopid)];
          }
          //仓库
          if(strtoupper($houseid) != 'ALL')
          {
             $_map[] = ['warehouse_id','EQ',intval($houseid)];
          }
          if($word)
          {
             $_map[] = ['admin_name|nickname|s_phone','LIKE', '%' . $word . '%'];
          }
          $arrList = AdminModel::where($_map)->order('id desc')->limit($offset, $limit)->select();
          $arrCount = AdminModel::where($_map)->count();
          if($arrCount <= 0 || count($arrList) <= 0)
          {
             return json(['err' => '1', 'msg' => '当前页下的数据已显示完毕！']);
          }
          foreach($arrList as $k => &$v)
          {
              $v['group_name'] = AuthGroup::getGroupName(intval($v['group_id']));
              $v['sex'] = __sex($v['sex']);
              $v['status'] = intval($v['status']) > 0 ? '启用' : '禁用';
              $v['shop_name'] = !$v['shop_id'] ? '总后台管理人员' : ShopsModel::getShopName($v['shop_id'],true);
              $v['warehouse_name'] = !$v['warehouse_id'] ? '总后台管理人员' : StoreWarehouseModel::getHouseName($v['warehouse_id']);
          }
          return json(['err' => '0', 'page' => $page , 'limit' => $limit, 'total' => $arrCount,'list' => $arrList]);
        }else{
          $page = intval($this->request->get('p')); //url 页码参数  ?p=1
          $this->assign([
             'page' => $page > 0 ? $page : '1',
             'group' => AuthGroup::getGroupList(),
             'shop_list' => ShopsModel::getSelectShopList(),
             'house_list' => StoreWarehouseModel::getSelectHouseList(0,'id,name,shop_id',true),
          ]);
          return $this->view->fetch();
        }
    }

    /*
      增加
    */
    public function add()
    {
        if($this->request->isAjax())
        {
            $v_name = trim($this->request->post('v_name'));
            $v_nick_name = trim($this->request->post('v_nick_name'));
            $v_gid = intval($this->request->post('v_gid'));
            $v_status = intval($this->request->post('v_status'));
            $v_pwd = trim($this->request->post('v_pwd'));
            $v_safecode = trim($this->request->post('v_safecode'));
            $v_phone = trim($this->request->post('v_phone'));
            $v_sex = intval($this->request->post('v_sex'));
            $v_img = trim($this->request->post('v_img'));
            $v_shopid = intval($this->request->post('v_shopid'));
            $v_house = intval($this->request->post('v_house'));
            $v_ulogin_backend = intval($this->request->post('v_ulogin_backend')); //是否允许登录后台
            if(!$v_name || !$v_pwd)
            {
              return json(['err' => '1', 'msg' => '登录账号或密码不能为空']);
            }
            //登录账号重复判断
            $_count = AdminModel::where(['admin_name' => $v_name])->order('id desc')->count();
            if($_count > 0)
            {
              return json(['err' => '2', 'msg' => '登录账号出现重复']);
            }
            $v_pwd = __re_md5($v_pwd);
            $iData = [
                'admin_name' => $v_name, //登录用户名
                'nickname' => $v_nick_name, //真实姓名
                'admin_pwd' => $v_pwd, //登录密码
                'img' => $v_img, //头像
                'sex' => $v_sex, //性别，1男2女0未知
                's_phone' => $v_phone, //手机号码
                'group_id' => $v_gid, //用户角色与cms_auth_group关联
                'shop_id' => $v_shopid, //所属门店
                'clinic_id' => '0', //所属门店诊所ID
                'status' => $v_status, //状态，1正常0停用
                'admin_lg_time' => '0', //最后登陆时间
                'warehouse_id' => $v_house, //分配的仓库id，仓库人员使用
                'create_time' => time(), //创建时间
            ];
            if($v_shopid)
            {
               $shopInfo = ShopsModel::getShopInfo($v_shopid);
               $iData['shop_id'] = (int)$shopInfo['id'];
               $iData['clinic_id'] = (int)$shopInfo['clinic_id'];
            }
            $newId = AdminModel::insertGetId($iData);
            return json(['err' => '0', 'msg' => '系统管理员账号添加成功']);
        }else{
           $this->assign([
            'group' => AuthGroup::getGroupList(),
            'shop_list' => ShopsModel::getSelectShopList(),
            'house_list' => StoreWarehouseModel::getSelectHouseList(0,'id,name,shop_id',true),
           ]);
           return $this->view->fetch();
        }
    }
    /*
      修改
    */
    public function edit()
    {
        if($this->request->isAjax())
        {
            $v_oldname = trim($this->request->post('v_oldname'));
            $v_id = intval($this->request->post('v_id'));
            $v_name = trim($this->request->post('v_name'));
            $v_nick_name = trim($this->request->post('v_nick_name'));
            $v_gid = intval($this->request->post('v_gid'));
            $v_status = intval($this->request->post('v_status'));
            $v_pwd = trim($this->request->post('v_pwd'));
            $v_safecode = trim($this->request->post('v_safecode'));
            $v_phone = trim($this->request->post('v_phone'));
            $v_sex = intval($this->request->post('v_sex'));
            $v_img = trim($this->request->post('v_img'));
            $v_shopid = intval($this->request->post('v_shopid'));
            $v_house = intval($this->request->post('v_house'));
            $v_ulogin_backend = intval($this->request->post('v_ulogin_backend')); //是否允许登录后台
            if(!$v_name)
            {
              return json(['err' => '1', 'msg' => '登录账号不能为空']);
            }
            //登录账号重复判断
            if($v_oldname != $v_name)
            {
              $_count = AdminModel::where(['admin_name' => $v_name])->order('id desc')->count();
              if($_count > 0)
              {
                return json(['err' => '2', 'msg' => '登录账号出现重复']);
              }
            }
            $iData = [
                'admin_name' => $v_name, //登录用户名
                'nickname' => $v_nick_name, //真实姓名
                'img' => $v_img, //头像
                'sex' => $v_sex, //性别，1男2女0未知
                's_phone' => $v_phone, //手机号码
                'group_id' => $v_gid, //用户角色与cms_auth_group关联
                'status' => $v_status, //状态，1正常0停用
                'shop_id' => $v_shopid, //所属门店
                'clinic_id' => '0', //所属门店诊所ID
                'warehouse_id' => $v_house, //分配的仓库id，仓库人员使用
            ];
            if($v_shopid)
            {
               if($v_id <= 1) //超级管理员禁止修改到门店去了
               {
                 $iData['shop_id'] = 0;
                 $iData['clinic_id'] = 0;
               }else{
                 $shopInfo = ShopsModel::getShopInfo($v_shopid);
                 $iData['shop_id'] = (int)$shopInfo['id'];
                 $iData['clinic_id'] = (int)$shopInfo['clinic_id'];
               }
            }
            if($v_pwd)
            {
               $iData['admin_pwd'] = __re_md5($v_pwd);
            }
            AdminModel::where(['id' => $v_id])->update($iData);
            return json(['err' => '0', 'msg' => '系统管理员账号信息修改成功']);
        }else{
           $id = intval($this->request->get('id'));
           $info = AdminModel::where(['id' => $id])->order('id desc')->find();
           if(!$info)
           {
             $this->redirect('admin/admin/index', '', 302);
           }
           $this->assign([
            'group' => AuthGroup::getGroupList(),
            'info' => $info,
            'shop_list' => ShopsModel::getSelectShopList(),
            'house_list' => StoreWarehouseModel::getSelectHouseList(0,'id,name,shop_id',true),
           ]);
           return $this->view->fetch();
        }
    }
    /*
      删除
    */
    public function del()
    {
        if($this->request->isAjax())
        {
            $id = intval($this->request->post('id'));
            $res = AdminModel::where(['id' => $id])->update(['is_del' => '1']);
            if($res)
            {
              return json(['err' => '0' , 'msg' => '相关数据已删除成功！']);
            }else{
              return json(['err' => '1' , 'msg' => '已删除' . $res . '条相关数据！']);
            }
        }
        $this->redirect('admin/admin/index', '', 302);
    }

}

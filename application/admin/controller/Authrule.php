<?php
namespace app\admin\controller;

use app\common\model\AuthRule as AuthRuleModel;
use think\facade\Cache;
use think\facade\Url;
use szktor\SystemDict; //系统公共字典
use szktor\Tree;

class Authrule extends BaseAdmin
{
	protected $noNeedLogin = [];
    protected $noNeedRight = [];
    protected $pageNum = 2000;   //默认分页显示的数量
    
    //初始化
    public function initialize()
    {
		parent::initialize();
		$this->assign([
			'menu_tag' => 'menu_one', //菜单组标记，与系统菜单节点相对应
			'module_type_list' => SystemDict::loadSystemRuleModuleType(), //菜单模块类型列表
		]);
    }

    public function index()
    {
        if($this->request->isAjax())
        {
          $page = intval($this->request->post('page'));
		  $vmodtype = trim($this->request->post('vmodtype'));
          $vtype = trim($this->request->post('vtype'));
          $word = trim($this->request->post('word'));
		  
          //分页
          $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
          $offset = intval(($page - 1) * intval($limit));  //开始位置
          //条件
          $_map = [];
          //模块类型
          if(strtoupper($vmodtype) != 'ALL')
          {
             $_map[] = ['module_type', 'EQ', trim($vmodtype)];
          }
          //类型
          if(strtoupper($vtype) != 'ALL')
          {
             $_map[] = ['type', 'EQ', trim($vtype)];
          }
          if($word)
          {
             $_map[] = ['title|url|remark','LIKE', '%' . $word . '%'];
          }
          $arrList = AuthRuleModel::where($_map)->order('sort desc')->limit($offset, $limit)->select();
          $arrCount = AuthRuleModel::where($_map)->count();
          if($arrCount <= 0 || count($arrList) <= 0)
          {
             return json(['err' => '1', 'msg' => '当前页下的数据已显示完毕！']);
          }
          foreach($arrList as $k => $v)
          {
              $arrList[$k]['icon'] = !$v['icon'] ? '/' : trim($v['icon']);
              $arrList[$k]['status'] = intval($v['status']) > 0 ? '启用' : '禁用';
          }
          //树形
          if(strtoupper($vtype) == 'ALL' && !$word && count($arrList) > 1)
          {
            $ruleList = $arrList->toArray();
            $tree = Tree::instance();
            $tree->init($ruleList);
            $arrList = $tree->getTreeList($tree->getTreeArray(0), 'title');
          }

          return json(['err' => '0', 'page' => $page , 'limit' => $limit, 'total' => $arrCount,'list' => $arrList]);
        }else{
          $page = intval($this->request->get('p')); //url 页码参数  ?p=1
          $this->assign('page', $page > 0 ? $page : '1');
          return $this->view->fetch();
        }
    }

    /*
      增加
    */
    public function add()
    {
        if($this->request->isAjax())
        {
            $v_title = trim($this->request->post('v_title'));
            $v_icon = trim($this->request->post('v_icon'));
            $v_pid = intval($this->request->post('v_pid'));
            $v_type = trim($this->request->post('v_type'));
            $v_url = trim($this->request->post('v_url'));
            $v_sort = intval($this->request->post('v_sort'));
            $v_status = intval($this->request->post('v_status'));
			$v_module_type = trim($this->request->post('v_module_type'));
            $v_remark = trim($this->request->post('v_remark'));
            if(!$v_title || !$v_url)
            {
              return json(['err' => '1', 'msg' => '标题或URL路由为空']);
            }
            //URL重复判断
            $_count = AuthRuleModel::where(['url' => $v_url])->order('id desc')->count();
            if($_count > 0)
            {
              return json(['err' => '2', 'msg' => 'URL路由出现重复']);
            }
            $iData = [
                'type' => $v_type, //menu为菜单,file为权限节点
                'pid' => $v_pid, //父ID
                'title' => $v_title, //规则标题
                'url' => strtolower($v_url), //URL规则名称地址
                'icon' => $v_icon, //图标
				'module_type' => $v_module_type, //菜单模块类型,前台=index_menu,后台=admin
                'remark' => $v_remark, //备注
                'create_time' => time(), //创建时间
                'update_time' => '0', //更新时间
                'sort' => $v_sort, //排序值越大越靠前显示
                'status' => $v_status, //状态1可用0禁用
            ];
            $newId = AuthRuleModel::insertGetId($iData);
            return json(['err' => '0', 'msg' => '权限菜单节点添加成功']);
        }else{
           $this->assign('rulelist',AuthRuleModel::getRuleList());
           return $this->view->fetch();
        }
    }
    /*
      修改
    */
    public function edit()
    {
        if($this->request->isAjax())
        {
            $v_oldurl = trim($this->request->post('v_oldurl'));
            $v_id = intval($this->request->post('v_id'));
            $v_title = trim($this->request->post('v_title'));
            $v_icon = trim($this->request->post('v_icon'));
            $v_pid = intval($this->request->post('v_pid'));
            $v_type = trim($this->request->post('v_type'));
            $v_url = trim($this->request->post('v_url'));
            $v_sort = intval($this->request->post('v_sort'));
            $v_status = intval($this->request->post('v_status'));
			$v_module_type = trim($this->request->post('v_module_type'));
            $v_remark = trim($this->request->post('v_remark'));
            if(!$v_title || !$v_url)
            {
              return json(['err' => '1', 'msg' => '标题或URL路由为空']);
            }
            //URL重复判断
            if($v_oldurl != $v_url)
            {
                $_count = AuthRuleModel::where(['url' => $v_url])->order('id desc')->count();
                if($_count > 0)
                {
                  return json(['err' => '2', 'msg' => 'URL路由出现重复']);
                }
            }
            $iData = [
                'type' => $v_type, //menu为菜单,file为权限节点
                'pid' => $v_pid, //父ID
                'title' => $v_title, //规则标题
                'url' => strtolower($v_url), //URL规则名称地址
                'icon' => $v_icon, //图标
                'remark' => $v_remark, //备注
				'module_type' => $v_module_type, //菜单模块类型,前台=index_menu,后台=admin
                'update_time' => time(), //更新时间
                'sort' => $v_sort, //排序值越大越靠前显示
                'status' => $v_status, //状态1可用0禁用
            ];
            AuthRuleModel::where(['id' => $v_id])->update($iData);
            return json(['err' => '0', 'msg' => '权限菜单节点信息修改成功']);
        }else{
           $id = intval($this->request->get('id'));
           $info = AuthRuleModel::where(['id' => $id])->order('id desc')->find();
           if(!$info)
           {
             $this->redirect('admin/authrule/index', '', 302);
           }
           $this->assign('rulelist',AuthRuleModel::getRuleList());
           $this->assign('info',$info);
           return $this->view->fetch();
        }
    }
    /*
      删除
    */
    public function del()
    {
        if($this->request->isAjax())
        {
            $id = intval($this->request->post('id'));
            $res = AuthRuleModel::where(['id' => $id])->delete();
            if($res)
            {
              return json(['err' => '0' , 'msg' => '相关数据已删除成功！']);
            }else{
              return json(['err' => '1' , 'msg' => '已删除' . $res . '条相关数据！']);
            }
        }
        $this->redirect('admin/authrule/index', '', 302);
    }
    /*
      设置排序
    */
    public function setsort()
    {
        if($this->request->isAjax())
        {
            $id = intval($this->request->post('id'));
            $val = intval($this->request->post('val'));
            $res = AuthRuleModel::where(['id' => $id])->update(['sort' => $val]);
            if($res)
            {
              return json(['err' => '0' , 'msg' => '排序数值已设置成功']);
            }else{
              return json(['err' => '1' , 'msg' => '设置排序值失败']);
            }
        }
        $this->redirect('admin/authrule/index', '', 302);
    }
}

<?php
namespace app\index\controller;

use szktor\SystemDict; //系统字典
use szktor\ClinicDict; //诊所门店公共字典

/*
* 前台-xxx功能
*/
class AAAA extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = [];  //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
    }
    
    //首页
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            try
            {
                $data = $this->request->only(['page'],'post');
                
                //条件
                $map = [
                    ['is_del','=','0'],
                    ['admin_id','=', $this->admin['id']],
                    ['clinic_id','=', $this->admin['clinic_id']],
                ];
                $map = [
                    ['title|url','like','%' . $data['searchword'] . '%'],
                ];
                $this->pageNum = 15;
                $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
                
                $list = AuthRule::where($map)->field('id,title,url,icon,remark,create_time')
                        ->order('sort desc')->paginate($limit,false,['page' => $data['page'], 'var_page' => 'page']);
                if (count($list) <= 0) {
                    return json(['err' => '2', 'msg' => '当前页下的数据已显示完毕！']);
                }
                foreach($list as $k => &$v)
                {
                    
                }
                return json([
                    'err' => '0',
                    'page' => $list->currentPage(),
                    'limit' => $limit,
                    'total' => $list->total(),
                    'list' => $list->items(),
                ]);
            }catch (\think\Exception $e){ // use think\Exception;
               return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
            }
        }
        $this->assign([
            'word' => '',
        ]);
        return $this->view->fetch();
    }

    //添加
    public function add()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            try
            {
            }catch (\think\Exception $e){ // use think\Exception;
               json(return ['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
            }
        }
        $this->assign(['aa' => '11']);
        return $this->view->fetch();
    }

    //修改
    public function edit()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            try
            {
            }catch (\think\Exception $e){ // use think\Exception;
               return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
            }
        }
        $this->assign(['aa' => '11']);
        return $this->view->fetch();
    }

    //删除
    public function del()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            $data = $this->request->only(['id']);
            if(intval($data['id']) <= 0){
                return json(['err' => '2', 'msg' => '参数错误，请重试']);
            }
            //条件
            $map = [['id','=',intval($data['id'])],['clinic_id','=',$this->admin['clinic_id']]];
            
            //$r = SystemAttachment::where($map)->update(['is_del' => '1']);
            //$r = SystemAttachment::where($map)->delete();

            if(!$r) return json(['err' => '3', 'msg' => '删除失败，请重试']);
            return json(['err' => '0', 'msg' => '删除成功']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
}

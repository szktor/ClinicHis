<?php
namespace app\index\controller\dictionary;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表
/*
* 前台-字典维护主页
*/
class Index extends BaseIndex
{
    public function index($tabname = '')
    {
		$this->assign([
            'tabname' => strtolower($tabname), //自动点击的tab
            'zdwh_all_list' => ClinicDict::getDictBigCateList(), //字典维护大类列表
        ]);
        return $this->view->fetch();
    }

}

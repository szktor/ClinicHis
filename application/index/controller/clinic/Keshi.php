<?php
namespace app\index\controller\clinic;
use app\index\controller\BaseIndex;
use app\common\model\Clinic;
use szktor\SystemDict; //系统字典
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表数据

/*
* 前台-科室 设置
*/
class Keshi extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = ['info','del'];  //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
    }
    
    //首页
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            $data = $this->request->only(['pid','keyword','status','page'],'post');
            //条件
            $map = [['clinic_id','=', $this->admin['clinic_id']]];
            if(isset($data['keyword']) && $data['keyword'] != '')
            {
                $map[] = ['name|bianma|address|remarks','like','%' . $data['keyword'] . '%'];
            }
            if(isset($data['pid']) && strtoupper($data['pid']) != 'ALL')
            {
                $map[] = ['pid','=',(int)$data['pid']];
            }
            if(isset($data['status']) && strtoupper($data['status']) != 'ALL')
            {
                $map[] = ['status','=',(int)$data['status']];
            }
            $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
            $list = Clinic::getSplitDb($this->admin['clinic_id'],'clinic_keshi')->where($map)
                    ->field('*')->order('sort desc')->paginate($limit,false,['page' => $data['page'], 'var_page' => 'page']);
            if (count($list) <= 0) {
                return json(['code' => '1', 'msg' => '当前页下的数据已显示完毕！', 'count' => '0', 'data' => []]);
            }
            $res = ['code' => 0, 'msg' => '', 'count' => $list->total(), 'data' => $list->items()];
            $kssxArr = ClinicDict::getKeShiAttrList(); //科室属性列表
            foreach ($res['data'] as $key => &$val) {
                //上级科室
                $val['pid_txt'] = '';
                //获取科室信息
                $ksInfo = ClinicDictsheet::getClinicKeShiInfo($val['pid'],$this->admin['clinic_id']);
                if($ksInfo) $val['pid_txt'] = $ksInfo['name'];
                //科室属性
                $val['attr_txt'] = _getDictNameTxt($val['attr'],$kssxArr);
                $val['create_time'] = $val['create_time'] > 0 ? date('Y-m-d H:i:s',$val['create_time']) : '/';
            }
            return json($res);
        }
        $this->assign([
            'kssx_list' => ClinicDict::getKeShiAttrList(), //科室属性列表
            'keshi_list' => ClinicDictsheet::getClinicKeShiList($this->admin['clinic_id']), //获取科室列表 Select 选择器使用
        ]);
        return $this->view->fetch();
    }
    //添加+修改
    public function info($id = 0)
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            $data = $this->request->only(['id','name','bianma','pid','attr','sort','open_yuyue','address','status','remarks'],'post');
            if(!isset($data['id']))
            {
                return json(['err' => '2', 'msg' => '对不起，参数错误']); 
            }
            if(!isset($data['name']) || $data['name'] == '')
            {
                return json(['err' => '2', 'msg' => '请输入科室名称']); 
            }
            if(!isset($data['bianma']) || $data['bianma'] == '')
            {
                return json(['err' => '2', 'msg' => '请输入科室编号']); 
            }
            try
            {
                $info =  Clinic::getSplitDb($this->admin['clinic_id'],'clinic_keshi')
                         ->where(['id' => intval($data['id']), 'clinic_id' => $this->admin['clinic_id']])->find();
                unset($data['id']);
                if($info) //信息已存在
                {
                    $editRes = Clinic::getSplitDb($this->admin['clinic_id'],'clinic_keshi')->where(['id' => $info['id']])->update($data);
                }else{ //信息不存在
                    $data['clinic_id'] = $this->admin['clinic_id']; //诊所门店表的id
                    $data['create_time'] = time(); //创建时间
                    $editRes = Clinic::getSplitDb($this->admin['clinic_id'],'clinic_keshi')->insertGetId($data);
                }
                unset($info);
                if(!$editRes) return json(['err' => '5', 'msg' => '科室信息保存失败']);
                return json(['err' => '0', 'msg' => '科室信息已保存成功']);
            }catch (\think\Exception $e){ // use think\Exception;
               return json(['err' => '6', 'msg' => '失败：' . $e->getMessage()]);
            }
        }
    }
    //删除
    public function del()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        $data = $this->request->only(['id']);
        if(intval($data['id']) <= 0){
            return json(['err' => '2', 'msg' => '参数错误，请重试']);
        }
        //条件
        $map = [['id','in',$data['id']],['clinic_id','=',$this->admin['clinic_id']]];
        $r = Clinic::getSplitDb($this->admin['clinic_id'],'clinic_keshi')->where($map)->update(['is_del' => '1']);
        if(!$r) return json(['err' => '3', 'msg' => '删除失败，请重试']);
        return json(['err' => '0', 'msg' => '删除成功']);
    }
}

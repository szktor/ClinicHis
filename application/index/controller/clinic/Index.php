<?php
namespace app\index\controller\clinic;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表
/*
* 前台-诊所设置主页
*/
class Index extends BaseIndex
{
    public function index($tabname = '')
    {
		$this->assign([
            'tabname' => strtolower($tabname), //自动点击的tab
            'zssz_all_list' => ClinicDict::getDictClinicConfigList(), //诊所设置大类列表
        ]);
        return $this->view->fetch();
    }

}

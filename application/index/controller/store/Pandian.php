<?php
namespace app\index\controller\store;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表数据
use app\common\model\ClinicConfig;
use szktor\Random;
use szktor\MyExcel;
use think\facade\Env;

/*
* 前台-库房管理 -> 盘点管理
* index/store.pandian/index
*/
class Pandian extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = ['save_info']; //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
    }

    //首页
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            try
            {
                $post = $this->request->only(['create_time','keyword','status','page'],'post');
                //条件
                $map = [
                    ['clinic_id','=', $this->admin['clinic_id']],
                ];
                if(isset($post['status']) && $post['status'] != '')
                {
                    if(strtoupper($post['status']) != 'ALL') $map[] = ['status','=',(int)$post['status']];
                }
                if(isset($post['keyword']) && $post['keyword'] != '')
                {
                    $map[] = ['pandian_sn|add_name|confim_name|remarks','like','%' . $post['keyword'] . '%'];
                }
                //日期
                $rkDate = $this->getWhereDate($post['create_time']);
                if($rkDate) $map[] = ['create_time',['>=',$rkDate[0]],['<=',$rkDate[1]],'AND'];
                //分页
                $page = isset($post['page']) ? $post['page'] : 1;
                $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
                $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->where($map)->field('*')
                        ->order('id desc')->paginate($limit,false,['page' => $page, 'var_page' => 'page']);
                if (count($list) <= 0) {
                    return json(['code' => '1', 'msg' => '当前页下的数据已显示完毕！', 'count' => '0', 'data' => []]);
                }
                $res = ['code' => 0,'msg' => '', 'count' => $list->total(), 'data' => $list->items()];
                foreach ($res['data'] as $key => &$val) {
                    $val['create_time'] = $val['create_time'] > 0 ? date('Y-m-d',$val['create_time']) : '';
                }
                return json($res);
            }catch (\think\Exception $e){ // use think\Exception;
                return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
            }
        }
        $this->assign([
            'aa' => 'aa',
        ]);
        return $this->view->fetch();
    }

    //添加+修改 index/store.pandian/info
    public function info($id = 0)
    {
        if($this->request->isAjax())
        {
            try
            {
                $data = $this->request->only(['id','drugtype','keyword','zeronoshow'],'post');
                $oldId = isset($data['id']) ? intval($data['id']) : 0;
                //条件
                $map = $map1 = [['clinic_id','=', $this->admin['clinic_id']]];
                if(isset($data['keyword']) && $data['keyword'] != '')
                {
                    $map[] = ['name|jd_name|bianma|py_code|wb_code|changjia','like','%' . $data['keyword'] . '%'];
                    $map1[] = ['name|py_code|wb_code|changjia','like','%' . $data['keyword'] . '%'];
                }
                if(isset($data['drugtype']) && $data['drugtype'] != '')
                {
                    //xy=西药,zcy=中成药,zyyp=中药饮片,zykl=中药颗粒,cl=材料
                    if(strtoupper($data['drugtype']) != 'ALL')
                    {
                        $map1[] = ['xm_type','=',$data['drugtype']];
                        if($data['drugtype'] == 'xy') //西药
                        {
                            $map[] = ['drugs_type','=','0'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'zcy') //中成药
                        {
                            $map[] = ['drugs_type','=','0'];
                            $map[] = ['yp_type','=','1'];
                        }
                        if($data['drugtype'] == 'zyyp') //中药饮片
                        {
                            $map[] = ['drugs_type','=','1'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'zykl') //中药颗粒
                        {
                            $map[] = ['drugs_type','=','1'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'cl') //材料
                        {
                            $map[] = ['drugs_type','=','2'];
                        }
                    }      
                }
                if(isset($data['zeronoshow']) && (int)$data['zeronoshow'] > 0)
                {
                    $map[] = ['stock','>','0'];
                    $map1[] = ['stock','>','0'];
                }

                if($oldId > 0)
                {
                    $map1[] = ['pandian_id','=',$oldId];
                    $field = '*';
                    $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')
                            ->where($map1)->field($field)->order('stock desc')->select();
                }else{
                    $field = 'id,name,spec,huogui_no,py_code,wb_code,changjia,cost_price,price,stock,drugs_type,yp_type,kc_unit,is_cl,cl_price,zj_unit,ls_unit,jl_num,jl_unit,zj_num';
                    $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_drugs')
                            ->where($map)->field($field)->order('stock desc')->select();
                }
                if (count($list) <= 0) {
                    return json(['err' => '1', 'msg' => '暂无数据', 'data' => []]);
                }
                if($oldId <= 0)
                {
                    $xy_zjunit_list = ClinicDict::getDrugsZhiJiUnitList(); //西药 - 制剂单位
                    $xy_kcunit_list = ClinicDict::getDrugsKuCunUnitList(); //西药 - 库存单位
                    $zy_kcunit_list = ClinicDict::getZhongYaoUnitList(); //中药(销售单位+库存单位)、材料(最小单位+库存单位)
                    foreach ($list as $key => &$val) {
                        //类型,0=西药,1=中药,2=材料
                        if($val['drugs_type'] == 0) //西药
                        {
                            $val['unit'] = _getDictNameTxt($val['kc_unit'],$xy_kcunit_list);
                            if((int)$val['is_cl'] > 0)
                            {
                                $val['price'] = $val['cl_price'];
                                $val['unit'] = _getDictNameTxt($val['zj_unit'],$xy_zjunit_list);
                            }
                            $val['spec'] = _getXiYaoSpecText($val);
                        }
                        //中药
                        if($val['drugs_type'] == 1)
                        {
                            $val['unit'] = _getDictNameTxt($val['ls_unit'],$xy_zjunit_list);
                        }
                        //材料
                        if($val['drugs_type'] == 2)
                        {
                            $val['unit'] = _getDictNameTxt($val['ls_unit'],$zy_kcunit_list);
                        }
                        $val['drugs_id'] = $val['id'];
                        $val['stock_after'] = $val['stock'];
                        $val['cost_price_total'] = _formatNumber('0');
                        $val['price_total'] = _formatNumber('0');
                        $val['xm_type'] = $this->getXmType($val);
                    }
                }else{
                    foreach ($list as $key => &$val) {
                        $val['cost_price_total'] = _formatNumber($val['cost_price_total']);
                        $val['price_total'] = _formatNumber($val['price_total']);
                    }
                }
                return json(['err' => '0', 'msg' => '加载数据成功', 'data' => $list]);
            }catch (\think\Exception $e){ // use think\Exception;
                return json(['err' => '500', 'msg' => '错误：' . $e->getMessage(), 'data' => []]);
            }
        }else{
            $info = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')
                    ->where(['id' => intval($id), 'clinic_id' => $this->admin['clinic_id']])->find();
            $tableList = [];
            if($info){
                $info['create_time'] = $info['create_time'] > 0 ? date('Y-m-d',$info['create_time']) : '';
                $tableList = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')
                           ->where(['pandian_id' => $info['id']])->order('id asc')->select();
                foreach ($tableList as $key => &$val) {
                    $val['create_time'] = $val['create_time'] > 0 ? date('Y-m-d',$val['create_time']) : '';
                }
            }
            $this->assign([
                'info' => $info,
                'table_list' => $tableList, //子项目列表
                'ypkc_type_list' => ClinicDict::getKuCunXmTypeList(), //库存药品项目类型
            ]);
            return $this->view->fetch();
        }
    }

    //保存盘点信息 index/store.pandian/save_info
    public function save_info()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '参数错误，无法执行']);
        }
        $data = $this->request->only([
            'id','all_cost_price','all_price','create_time','remarks','table_list',
        ],'post');
        if(!isset($data['id']))
        {
            return json(['err' => '1', 'msg' => '参数错误，无法执行']);
        }
        if(!isset($data['table_list']) || count($data['table_list']) <= 0)
        {
            return json(['err' => '2', 'msg' => '对不起，还未添加盘点项目明细']);
        }
        $info = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')
                ->where(['id' => intval($data['id']), 'clinic_id' => $this->admin['clinic_id']])->find();
        $now_time = time();
        ClinicDictsheet::beginTransClinic($this->admin['clinic_id']);
        try
        {
            if($info) //数据已存在
            {
                $sqlData = [
                    'total_num' => count($data['table_list']), //明细记录总数
                    'cost_price' => _formatNumber($data['all_cost_price']), //进货盈亏合计
                    'ls_price' => _formatNumber($data['all_price']), //零售盈亏合计
                    'add_name' => $this->getThisAdminName($this->admin), //制单人员
                    'confim_name' => '', //确认人员
                    'remarks' => $data['remarks'], //出入库备注
                    'status' => '0', //状态0未确认1已确认
                    'confim_time' => '0', //确认出入库时间
                    'create_time' => empty($data['create_time']) ? $now_time : strtotime($data['create_time']), //出入库时间
                ];
                $r1 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->where(['id' => $info['id']])->update($sqlData);
                $r2 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')->where(['pandian_id' => $info['id']])->delete();
                $entryId = $info['id'];
            }else{ //数据不存在
                $entrySn = $this->buildUniquePanDianSn('PD',4);
                $sqlData = [
                    'clinic_id' => $this->admin['clinic_id'], //诊所ID
                    'pandian_sn' => $entrySn, //盘点单号
                    'total_num' => count($data['table_list']), //明细记录总数
                    'cost_price' => _formatNumber($data['all_cost_price']), //进货盈亏合计
                    'ls_price' => _formatNumber($data['all_price']), //零售盈亏合计
                    'add_name' => $this->getThisAdminName($this->admin), //制单人员
                    'confim_name' => '', //确认人员
                    'remarks' => $data['remarks'], //出入库备注
                    'status' => '0', //状态0未确认1已确认
                    'confim_time' => '0', //确认出入库时间
                    'create_time' => empty($data['create_time']) ? $now_time : strtotime($data['create_time']), //出入库时间
                ];
                $entryId = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->insertGetId($sqlData);
                if(!$entryId)
                {
                    ClinicDictsheet::commitTransClinic($this->admin['clinic_id']);
                    return json(['err' => '3', 'msg' => '对不起，添加出错，请重试']);
                }
            }
            //明细数据
            $logData = [];
            foreach ($data['table_list'] as $key => $val) {
                $arrA = [
                    'clinic_id' => $this->admin['clinic_id'], //诊所ID
                    'pandian_id' => $entryId, //盘点点记录ID
                    'status' => '0', //状态0未确认1已确认
                    'create_time' => empty($data['create_time']) ? $now_time : strtotime($data['create_time']), //出入库时间
                ];
                $logData[] = array_merge($arrA,$val);
                unset($arrA);
            }
            $editRes = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')->insertAll($logData);
            ClinicDictsheet::commitTransClinic($this->admin['clinic_id']);
            unset($data,$info,$sqlData,$logData);
            return json(['err' => '0', 'msg' => '盘点数据已保存成功']);
        }catch (\think\Exception $e){ // use think\Exception;
            ClinicDictsheet::rollbackTransClinic($this->admin['clinic_id']);
            return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //查看详情
    public function detail($id = 0)
    {
        $info = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')
                ->where(['id' => intval($id), 'clinic_id' => $this->admin['clinic_id']])->find();
        if(!$info)
        {
            return $this->redirect(Url::build('index/store.pandian/index'), '', 302);
        }
        $tableList = [];
        $info['create_time'] = $info['create_time'] > 0 ? date('Y-m-d',$info['create_time']) : '';
        $tableList = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')
                   ->where(['pandian_id' => $info['id']])->order('id asc')->select();
        foreach ($tableList as $key => &$val) {
            $val['create_time'] = $val['create_time'] > 0 ? date('Y-m-d',$val['create_time']) : '';
            $val['cost_price_total'] = _formatNumber($val['cost_price_total']);
            $val['price_total'] = _formatNumber($val['price_total']);
        }
        $this->assign([
            'info' => $info,
            'table_list' => $tableList, //子项目列表
        ]);
        return $this->view->fetch();
    }
    //盘点确认
    public function confim()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            $data = $this->request->only(['id'],'post');
            if(!isset($data['id']) || intval($data['id']) <= 0)
            {
                return json(['err' => '1', 'msg' => '参数错误，无法执行']);
            }
            ClinicDictsheet::beginTransClinic($this->admin['clinic_id']);
            $map = [['id','in',$data['id']],['clinic_id','=',$this->admin['clinic_id']]];
            $res1 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->where($map)->update([
                'status' => '1', //状态0未确认1已确认
                'confim_name' => $this->getThisAdminName($this->admin), //确认人员
                'confim_time' => time(), //确认时间
            ]);
            $entrySn = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->where($map)->value('pandian_sn'); 
            $map1 = [['pandian_id','in',$data['id']],['clinic_id','=',$this->admin['clinic_id']]];
            $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')
                    ->where($map1)->field('*')->order('id asc')->select();
            $inoutLogArr = [];
            foreach ($list as $key => $val) {
                if($val['stock'] != $val['stock_after'])
                {
                    //重新设置项目库存
                    $map2 = ['id' => $val['drugs_id'], 'clinic_id' => $this->admin['clinic_id']];
                    $s1 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_drugs')->where($map2)->update(['stock' => $val['stock_after']]);
                    //出入库库存变动明细
                    $numStr = (float)$val['stock_after']-(float)$val['stock'];
                    $inoutLogArr[] = [
                        'clinic_id' => $val['clinic_id'], //诊所ID
                        'inout_sn' => $entrySn, //出入库单号
                        'type' => '2', //出入库方式0=入库,1=出库,2=盘点,3=发药,4=退药
                        'drugs_id' => $val['drugs_id'], //clinic_drugs表记录ID
                        'name' => $val['name'], //名称
                        'py_code' => $val['py_code'], //拼音码助记词
                        'wb_code' => $val['wb_code'], //五笔码助记词
                        'batch_sn' => '', //出入库批号
                        'num' => $numStr, //出入库数量
                        'unit' => $val['unit'], //单位
                        'price' => $val['cost_price'], //进货单价
                        'ls_price' => $val['price'], //零售单价
                        'except_time' => '', //有效期
                        'changjia' => $val['changjia'], //厂家生产厂商
                        'remarks' => $val['remarks'], //备注
                        'create_time' => $val['create_time'], //出入库时间
                    ];
                    unset($map2,$numStr,$s1);
                }
            }
            $res2 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')->where($map1)->update([
                'status' => '1', //状态0未确认1已确认
            ]);
            if(count($inoutLogArr))
            {
                $res3 = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_inout_log')->insertAll($inoutLogArr);
            }
            unset($inoutLogArr);
            ClinicDictsheet::commitTransClinic($this->admin['clinic_id']);
            return json(['err' => '0', 'msg' => '盘点确认成功']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //盘点删除
    public function del()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            $data = $this->request->only(['id'],'post');
            if(!isset($data['id']) || intval($data['id']) <= 0)
            {
                return json(['err' => '1', 'msg' => '参数错误，无法执行']);
            }
            ClinicDictsheet::beginTransClinic($this->admin['clinic_id']);
            $map = [['id','in',$data['id']],['clinic_id','=',$this->admin['clinic_id']]];
            $res1 = false !== ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian')->where($map)->delete();
            $map1 = [['pandian_id','in',$data['id']],['clinic_id','=',$this->admin['clinic_id']]];
            $res2 = false !== ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')->where($map1)->delete();
            $res = $res1 && $res2;
            ClinicDictsheet::checkTransClinic($res,$this->admin['clinic_id']);
            if(!$res) return json(['err' => '2', 'msg' => '盘点数据删除失败，请重试']);
            return json(['err' => '0', 'msg' => '盘点数据删除成功']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }

    //盘点数据导出为 Excel 
    public function save_excel()
    {
        $data = $this->request->post('pdata');
        $data = json_decode($data,true);
        print_r($data);
        $list = [];
        if(isset($data['table_list']) && count($data['table_list']) > 0)
        {
            $list = $data['table_list'];
        }
        try
        {
            $csvList = [];
            foreach ($list as $key => &$val) {
              $csvList[] = [
                  $key+1,
                  $val['drugs_id'],
                  $val['name'],
                  $val['spec'],
                  $val['huogui_no'],
                  $val['changjia'],
                  $val['cost_price'],
                  $val['price'],
                  $val['unit'],
                  $val['stock'],
                  $val['stock_after'],
                  $val['cost_price_total'],
                  $val['price_total'],
              ];
            }
            $headArr = ['序号', 'ID(勿删)', '名称', '规格','货柜号','生产厂商','进货单价(元)','零售单价(元)','单位','账面数量','实际数量', '进货盈亏(元)', '零售盈亏(元)']; //字段
            MyExcel::exportToExcel($headArr,$csvList,$this->clinic['short_name'] . '_库存盘点明细','库存盘点明细');
            die();
        }catch (\think\Exception $e){ // use think\Exception;
            return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //导入
    public function import_excel()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $post = $this->request->only(['id','path']);
            $post['path'] = isset($post['path']) ? urldecode($post['path']) : '';
            if(!$post['path'])
            {
                return json(['err' => '1', 'msg' => '对不起，参数有误']);
            }
            $excelFile = Env::get('root_path') . 'public' . $post['path'];
            if(!file_exists($excelFile))
            {
               return json(['err' => '2', 'msg' => '上传的文件不存在']);
            }
            //开始处理Excel
            $objPHPExcel = MyExcel::importFromExcel($excelFile);
            $excelArray = $objPHPExcel->getsheet(0)->toArray();   //转换为数组格式
            array_shift($excelArray);  //删除第一个数组(大标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }
            $arrList = $arrIds = [];
            foreach ($excelArray as $key => $val) {
                $drugsId = isset($val[1]) ? intval($val[1]) : 0;
                if($drugsId <= 0) continue;
                $arrIds[] = $drugsId;
                $arrList[$drugsId] = $val;
            }
            unset($excelArray);
            if(!count($arrList))
            {
                return json(['err' => '3', 'msg' => '导入的文件中数据(为空)无效']);
            }
            $arrIds = array_unique($arrIds);
            if(isset($post['id']) && intval($post['id']) > 0)
            {
                $map = [
                    ['clinic_id','=', $this->admin['clinic_id']],
                    ['pandian_id','=',intval($post['id'])],
                ];
                $field = '*';
                $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_pandian_log')
                        ->where($map)->field($field)->order('stock desc')->select();
            }else{
                $map = [
                    ['clinic_id','=', $this->admin['clinic_id']],
                    ['id','IN',implode(',',$arrIds)],
                ];
                $field = 'id,name,spec,huogui_no,py_code,wb_code,changjia,cost_price,price,stock,drugs_type,yp_type,kc_unit,is_cl,cl_price,zj_unit,ls_unit,jl_num,jl_unit,zj_num';
                $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_drugs')
                        ->where($map)->field($field)->order('stock desc')->select();
            }
            unset($arrIds);
            if (count($list) <= 0) {
                return json(['err' => '4', 'msg' => '导入的文件中数据匹配失败', 'data' => []]);
            }
            if(isset($post['id']) && intval($post['id']) <= 0)
            {
                $xy_zjunit_list = ClinicDict::getDrugsZhiJiUnitList(); //西药 - 制剂单位
                $xy_kcunit_list = ClinicDict::getDrugsKuCunUnitList(); //西药 - 库存单位
                $zy_kcunit_list = ClinicDict::getZhongYaoUnitList(); //中药(销售单位+库存单位)、材料(最小单位+库存单位)
                foreach ($list as $key => &$val) {
                    //类型,0=西药,1=中药,2=材料
                    if($val['drugs_type'] == 0) //西药
                    {
                        $val['unit'] = _getDictNameTxt($val['kc_unit'],$xy_kcunit_list);
                        if((int)$val['is_cl'] > 0)
                        {
                            $val['price'] = $val['cl_price'];
                            $val['unit'] = _getDictNameTxt($val['zj_unit'],$xy_zjunit_list);
                        }
                        $val['spec'] = _getXiYaoSpecText($val);
                    }
                    //中药
                    if($val['drugs_type'] == 1)
                    {
                        $val['unit'] = _getDictNameTxt($val['ls_unit'],$xy_zjunit_list);
                    }
                    //材料
                    if($val['drugs_type'] == 2)
                    {
                        $val['unit'] = _getDictNameTxt($val['ls_unit'],$zy_kcunit_list);
                    }
                    $val['drugs_id'] = $val['id'];
                    $val['stock_after'] = isset($arrList[$val['id']][10]) ? (int)$arrList[$val['id']][10] : $val['stock'];
                    $val['cost_price_total'] = _formatNumber('0');
                    $val['price_total'] = _formatNumber('0');
                    $val['xm_type'] = $this->getXmType($val);
                }
            }else{
                foreach ($list as $key => &$val) {
                    $val['cost_price_total'] = _formatNumber($val['cost_price_total']);
                    $val['price_total'] = _formatNumber($val['price_total']);
                    $val['stock_after'] = isset($arrList[$val['drugs_id']][10]) ? (int)$arrList[$val['drugs_id']][10] : $val['stock_after'];
                }
            }
            unset($arrList);
            return json(['err' => '0', 'msg' => '加载Excel数据成功', 'data' => $list]);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }

    /**
    *生成唯一字段值 唯一  盘点单号
    */
    private function buildUniquePanDianSn($pre = 'PD', $len = 4, $fields = 'pandian_sn')
    {
        $isExists = true;
        $tmpCode = '';
        do {
            $tmpCode = $pre . date('YmdH') . strtoupper(Random::numeric($len)); //数字
            $isExists = $this->checkUniqueCode($tmpCode,$fields,'clinic_pandian');
        } while ($isExists);
        return $tmpCode;
    }
}

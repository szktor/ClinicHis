<?php
namespace app\index\controller\store;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表
/*
* 前台-库房管理主页
*/
class Index extends BaseIndex
{
    public function index($tabname = '')
    {
		$this->assign([
            'tabname' => strtolower($tabname), //自动点击的tab
            'store_cate_list' => ClinicDict::getKuCunManagerCateList(), //库房管理 大类列表
        ]);
        return $this->view->fetch();
    }

}

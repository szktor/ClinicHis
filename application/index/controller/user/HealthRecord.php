<?php
namespace app\index\controller\user;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表数据
use app\common\model\ClinicUser; //客户表
use szktor\MyExcel;

/*
* 前台-客户管理 -> 健康档案管理
* index/user.health_record/index
*/
class HealthRecord extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = []; //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
        $this->pageNum = 20;
    }

    //首页 - index/user.health_record/index
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            //首页 和导出 共用方法
            $res = $this->afterSql();
            return json($res);
        }
        $this->assign([
            'ypkc_type_list' => ClinicDict::getKuCunXmTypeList(), //库存药品项目类型
        ]);
        return $this->view->fetch();
    }
    //库存index方法中的列表 导出Excel index/user.health_record/to_excel
    public function to_excel()
    {
        //首页 和导出 共用方法
        $res = $this->afterSql(true);
        $list = [];
        if(isset($res['code']) && $res['code'] == 0)
        {
            $list = $res['data'];
        }
        try
        {
            $csvList = [];
            foreach ($list as $key => &$val) {
              $csvList[] = [
                  $key+1,
                  $val['xm_type_txt'],
                  $val['name'],
                  $val['spec'],
                  $val['huogui_no'],
                  $val['changjia'],
                  $val['cost_price'],
                  $val['price'],
                  $val['ls_unit'],
                  $val['stock'],
              ];
            }
            $headArr = ['序号','药品类型','名称','规格','货柜号','生产厂家','进货价','零售价','单位','库存总量']; //字段
            MyExcel::exportToExcel($headArr,$csvList,$this->clinic['short_name'] . '_库存明细记录','库存明细记录');
            die();
        }catch (\think\Exception $e){ // use think\Exception;
            return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //首页 和导出 共用方法
    private function afterSql($saveExcel = false)
    {
        try
        {
            $data = $this->request->only(['drugtype','keyword','zeronoshow','page'],'post');
            if(!$saveExcel && !isset($data['page']))
            {
                return ['code' => '1', 'msg' => '参数错误，无法获取数据', 'count' => '0', 'data' => []];
            }
            //条件
            $map = [['clinic_id','=', $this->admin['clinic_id']]];
            if(isset($data['keyword']) && $data['keyword'] != '')
            {
                $map[] = ['wechat_name|name|user_number|phone|tel','like','%' . $data['keyword'] . '%'];
            }
            if(isset($data['drugtype']) && $data['drugtype'] != '')
            {
                if(strtoupper($data['drugtype']) != 'ALL')
                {
                    
                }      
            }
            if(isset($data['zeronoshow']) && (int)$data['zeronoshow'] > 0)
            {
                //$map[] = ['stock','>','0'];
            }
            $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
            $field = '*';
            //导出为Excel
            if($saveExcel)
            {
                $list = ClinicUser::where($map)->field($field)->order('id desc')->select();
            }else{
                $list = ClinicUser::where($map)->field($field)->order('id desc')
                        ->paginate($limit,false,['page' => $data['page'], 'var_page' => 'page']);
            }
            if (count($list) <= 0) {
                return ['code' => '3', 'msg' => '当前页下无数据', 'count' => '0', 'data' => []];
            }
            $res = [
                'err' => 0,
                'code' => 0,
                'msg' => '',
                'page' => !$saveExcel ? $list->currentPage() : $data['page'],
                'limit' => $limit,
                'count' => !$saveExcel ? $list->total() : count($list),
                'data' => !$saveExcel ? $list->items() : $list,
            ];
            foreach ($res['data'] as $key => &$val) {
                
            }
            return $res;
        }catch (\think\Exception $e){ // use think\Exception;
            return ['code' => '500', 'msg' => '错误：' . $e->getMessage(), 'count' => '0', 'data' => []];
        }
    }

    //查看会员详情 index/user.health_record/detail
    public function detail($uid = 0, $name = '')
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            try
            {
            //修改字段值
                $action = $this->request->post('action');
                if($action && strtolower($action) == 'edit')
                {
                    $post = $this->request->only(['id','field','value'],'post');
                    try
                    {
                        if(!isset($post['id']) || intval($post['id']) <= 0)
                        {
                            return json(['err' => '1', 'msg' => '参数错误，无法执行']);
                        }
                        if(!isset($post['field']) || empty($post['field']))
                        {
                            return json(['err' => '2', 'msg' => '参数错误，无法执行']);
                        }
                        if(!isset($post['value']))
                        {
                            return json(['err' => '3', 'msg' => '参数错误，无法执行']);
                        }
                        //条件
                        $map = [['id','=',$post['id']],['clinic_id','=', $this->admin['clinic_id']]];
                        $res = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_drugs')->where($map)
                                ->update([$post['field'] => $post['value']]);
                        if(!$res) return json(['err' => '2', 'msg' => '数据更新失败，请重试']);
                        return json(['err' => '0', 'msg' => '数据更新成功']);
                    }catch (\think\Exception $e){ // use think\Exception;
                        return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
                    }
                }
            //获取列表数据
                $data = $this->request->only(['drugtype','keyword','hghnoset','zeronoshow','page'],'post');
                if(!isset($data['page']))
                {
                    return json(['code' => '1', 'msg' => '参数错误，无法获取数据', 'count' => '0', 'data' => []]);
                }
                //条件
                $map = [['clinic_id','=', $this->admin['clinic_id']]];
                if(isset($data['keyword']) && $data['keyword'] != '')
                {
                    $map[] = ['name|jd_name|bianma|py_code|wb_code|changjia','like','%' . $data['keyword'] . '%'];
                }
                if(isset($data['drugtype']) && $data['drugtype'] != '')
                {
                    //xy=西药,zcy=中成药,zyyp=中药饮片,zykl=中药颗粒,cl=材料
                    if(strtoupper($data['drugtype']) != 'ALL')
                    {
                        if($data['drugtype'] == 'xy') //西药
                        {
                            $map[] = ['drugs_type','=','0'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'zcy') //中成药
                        {
                            $map[] = ['drugs_type','=','0'];
                            $map[] = ['yp_type','=','1'];
                        }
                        if($data['drugtype'] == 'zyyp') //中药饮片
                        {
                            $map[] = ['drugs_type','=','1'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'zykl') //中药颗粒
                        {
                            $map[] = ['drugs_type','=','1'];
                            $map[] = ['yp_type','=','0'];
                        }
                        if($data['drugtype'] == 'cl') //材料
                        {
                            $map[] = ['drugs_type','=','2'];
                        }
                    }      
                }
                if(isset($data['zeronoshow']) && (int)$data['zeronoshow'] > 0)
                {
                    $map[] = ['stock','>','0'];
                }
                if(isset($data['hghnoset']) && (int)$data['hghnoset'] > 0)
                {
                    $map[] = ['huogui_no','=',''];
                }
                $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
                $field = '*';
                $list = ClinicDictsheet::getSplitDb($this->admin['clinic_id'],'clinic_drugs')->where($map)->field($field)
                        ->order('id desc')->paginate($limit,false,['page' => $data['page'], 'var_page' => 'page']);
                if (count($list) <= 0) {
                    return json(['code' => '3', 'msg' => '当前页下无数据', 'count' => '0', 'data' => []]);
                }
                //库存药品项目类型
                $ypkc_type_list = ClinicDict::getKuCunXmTypeList();
                $xy_zjunit_list = ClinicDict::getDrugsZhiJiUnitList(); //西药 - 制剂单位
                $xy_kcunit_list = ClinicDict::getDrugsKuCunUnitList(); //西药 - 库存单位
                $zy_kcunit_list = ClinicDict::getZhongYaoUnitList(); //中药(销售单位+库存单位)、材料(最小单位+库存单位)
                $res = ['code' => 0, 'msg' => '', 'count' => $list->total(), 'data' => $list->items()];
                foreach ($res['data'] as $key => &$val) {
                    //类型,0=西药,1=中药,2=材料
                    if($val['drugs_type'] == 0) //西药
                    {
                        $val['ls_unit'] = _getDictNameTxt($val['kc_unit'],$xy_kcunit_list);
                        if((int)$val['is_cl'] > 0)
                        {
                            $val['price'] = $val['cl_price'];
                            $val['ls_unit'] = _getDictNameTxt($val['zj_unit'],$xy_zjunit_list);
                        }
                        $val['spec'] = _getXiYaoSpecText($val);
                    }
                    //中药
                    if($val['drugs_type'] == 1)
                    {
                        $val['ls_unit'] = _getDictNameTxt($val['ls_unit'],$xy_zjunit_list);
                    }
                    //材料
                    if($val['drugs_type'] == 2)
                    {
                        $val['ls_unit'] = _getDictNameTxt($val['ls_unit'],$zy_kcunit_list);
                    }
                    //库存药品项目类型
                    $val['xm_type_txt'] = _getDictNameTxt($this->getXmType($val),$ypkc_type_list);
                }
                return json($res);
            }catch (\think\Exception $e){ // use think\Exception;
                return json(['code' => '500', 'msg' => '错误：' . $e->getMessage(), 'count' => '0', 'data' => []]);
            }
        }
        //客户信息
        $user = ClinicUser::where([
            ['id','=',(int)$uid],
            ['clinic_id','=',$this->admin['clinic_id']],
        ])->find();
        if(!$user)
        {
            return $this->redirect(Url::build('index/user.health_record/index'), '', 302);
        }
        $this->assign([
            'user' => $user, //客户信息
        ]);
        return $this->view->fetch();
    }
    

}

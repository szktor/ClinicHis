<?php
namespace app\index\controller\user;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表
use app\common\model\ClinicUser; //客户表

/*
* 前台-客户管理主页
*/
class Index extends BaseIndex
{
    public function index($tabname = '')
    {
		$this->assign([
            'tabname' => strtolower($tabname), //自动点击的tab
            'kehu_cate_list' => ClinicDict::getKeHuGuanLiCateList(), //客户管理 大类列表
        ]);
        return $this->view->fetch();
    }

}

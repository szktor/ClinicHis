<?php
namespace app\index\controller;
use app\common\model\ClinicAdmin;
use app\common\model\ClinicAdminMessage;
use szktor\SystemDict; //系统字典
use szktor\ClinicDict; //诊所门店公共字典
/*
* 前台-门店人员 消息管理
*/
class AdminMessage extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = ['set_status'];  //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
    }
    
    //首页
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            $data = $this->request->only(['mtype','keyword','page'],'post');
            
            //条件
            $map = [['admin_id','=', $this->admin['id']]];
            if(isset($data['keyword']) && $data['keyword'] != '')
            {
                $map[] = ['msg_title|msg_body','like','%' . $data['keyword'] . '%'];
            }
            if(strtoupper($data['mtype']) != 'ALL')
            {
                $map[] = ['msg_type','=',$data['mtype']];
            }
            $limit = intval($this->pageNum) > 0 ? intval($this->pageNum) : 20;
            $list = ClinicAdminMessage::where($map)->field('id,from_id,from_name,msg_title,create_time')
                    ->order('id desc')->paginate($limit,false,['page' => $data['page'], 'var_page' => 'page']);
            if (count($list) <= 0) {
                return json(['code' => '1', 'msg' => '当前页下的数据已显示完毕！', 'count' => '0', 'data' => []]);
            }
            return json([
                'code' => 0,
                'msg' => '',
                'count' => $list->total(),
                'data' => $list->items(),
            ]);
        }
        //消息类型处理
        $msgTypeList = ClinicDict::getAdminMessageTypeList();
        foreach ($msgTypeList as $k => &$v) {
            $v['no_read_count'] = (int)ClinicAdminMessage::getMessageCount($this->admin['id'],$v['id'],'0');
        }
        $this->assign([
            'msg_type_list' => $msgTypeList,
        ]);
        return $this->view->fetch();
    }

    //查看详情
    public function detail($id = 0)
    {
        if(intval($id) <= 0)
        {
            return $this->redirect(Url::build('index/admin_message/index'), '', 302);
        }
        $info = ClinicAdminMessage::getOneInfo($id,$this->admin['id']);
        if(!$info || !isset($info['id']) || intval($info['id']) <= 0)
        {
            return $this->redirect(Url::build('index/admin_message/index'), '', 302);
        }
        //设置为已读
        $r = ClinicAdminMessage::setMessageStatus($this->admin['id'],$info['id'], 1);
        $this->assign(['info' => $info]);
        return $this->view->fetch();
    }

    //发送新消息
    public function add()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        




        
    }

    //删除
    public function del()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        $data = $this->request->only(['id']);
        if(intval($data['id']) <= 0){
            return json(['err' => '2', 'msg' => '参数错误，请重试']);
        }
        //条件
        $map = [['id','in',$data['id']],['admin_id','=',$this->admin['id']]];
        $r = ClinicAdminMessage::where($map)->delete();
        if(!$r) return json(['err' => '3', 'msg' => '删除失败，请重试']);
        return json(['err' => '0', 'msg' => '删除成功']);
    }

    //消息已读
    public function set_status()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        $data = $this->request->only(['tag','id']);
        if(strtolower($data['tag']) == 'readyall')
        {
            $r = ClinicAdminMessage::setMessageStatus($this->admin['id'],'', 1);
        }else{
            if(intval($data['id']) <= 0){
                return json(['err' => '2', 'msg' => '参数错误，请重试']);
            }
            $r = ClinicAdminMessage::setMessageStatus($this->admin['id'],$data['id'], 1);
        }
        if(!$r) return json(['err' => '3', 'msg' => '消息状态设置失败，请重试']);
        return json(['err' => '0', 'msg' => '消息状态设置成功']);
    }

}

<?php
namespace app\index\controller;

use app\common\model\ClinicAdmin;
use app\common\model\AuthRule;
use app\common\model\Clinic;
use app\common\model\ClinicAuthGroup;
use app\common\model\ClinicConfig;
use app\common\model\ClinicGroup;
use app\common\model\ClinicUser; //客户表
use think\Db;
use think\facade\Cache;
use think\facade\Cookie;
use think\facade\Session;
use think\facade\Request;
use think\facade\Url;
use think\Controller;
use think\facade\Env;
use szktor\Random;
use szktor\Tree;
use szktor\ClinicDict; //诊所门店公共字典
use szktor\SystemDict; //系统字典公共定义

/*
* 测试 模块
*/
class Test extends BaseIndex
{
    protected $noNeedLogin = ['set_uidpwd'];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = [];  //无需鉴权的方法，但需要登录

    //初始化
    public function initialize()
    {
        parent::initialize();
    }

	// index/test/index.html
    public function index()
    {
    	$arr = [
            'hzxx' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
            'xy' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
            'zy' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
            'cl' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
            'zlxm' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
            'zhxm' => ['excel_tpl' => 'sdfsds.xls', 'upload_url' => Url::build('index/base.import/inport_hzxx')],
        ];
    	/*
		$arr = [
			['title' => '标题01', 'width' => '100'],
			['title' => '标题02', 'width' => '150'],
			['title' => '标题03', 'width' => '200'],
			['title' => '标题04', 'width' => '250'],
		];
		*/
		exit(json_encode($arr,JSON_UNESCAPED_UNICODE));
		die("NOAUTH");
		$this->assign(['aa' => '11']);
        return $this->view->fetch();
    }
    /*
	* 功能：重置诊所账号的登录密码
	* index/test/set_uidpwd.html?adminid=1&pwd=12345678
	*/
    public function set_uidpwd($adminid = 0, $pwd = '123456')
    {
		//重置用户密码
		$r = ClinicAdmin::resetPassword($adminid, $pwd, rand(1000,9999));
		echo 'NewPassword:' . $pwd . '</br>';
        print_r($r);
    }
    /*
	* 功能：注册 测试 - 运行  init_clinic.sql
	* index/test/reg_initsql.html?clinic_id=1
	*/
    public function reg_initsql($clinic_id = 1)
    {
		$clinicId = (int)$clinic_id;
		$adminId = 66;
		$clinicAuthGroupId = 77;
		$sqlFile = Env::get('app_path').'index'.DS.'library'.DS.'init_clinic.sql';
		$sqlBody = _readFileContent($sqlFile);
		if($sqlBody)
		{
			//sql文件内变量 替换
			$sqlBody = str_replace('CLINICID',$clinicId,$sqlBody); //诊所ID
			$sqlBody = str_replace('ADMINID',$adminId,$sqlBody); //管理员ID
			$sqlBody = str_replace('ADMINGPID',$clinicAuthGroupId,$sqlBody); //管理员组ID
			$sqlArr = explode(';',$sqlBody);
			if(count($sqlArr))
			{
				foreach($sqlArr as $k => $v){
					$v = trim($v);
					if(!$v) continue; //为空不执行
					if(substr($v,0,2) == '--') continue; //注释不执行
					$sqlRes = ClinicAdmin::execute($v);
				}
			}
			unset($sqlFile,$sqlBody,$sqlArr);
		}
		exit('OK');
    }
	
    /*
	* 功能：连接分库 , 第一步就先创建一个分库：index/test/reg_initsql.html?clinic_id=1
	* index/test/link_split_db.html?clinicid=1
	*/
    public function link_split_db($clinicid = 0)
    {
		//# 连接分库   模型类名::getSplitDb($clinicId, $sheet_name)
		//如：Clinic::getSplitDb($clinicId, $sheet_name)->count();

		$r1 = false;//ClinicAdmin::getSplitDb($clinicid,'clinic_dictsheet')->fetchSql()->count();
		$r2 = ClinicAdmin::getSplitDb($clinicid,'clinic_dictsheet')->count();
        print_r($r1);
		
		echo '</br></br></br>';
		
        print_r($r2);
		
    }

    //快速生成病人 index/test/create_clinicuser.html?clinicid=1&num=80
    public function create_clinicuser($clinicid = 0, $num = 30)
    {
        $nameArr = ['刘敏瑜','余晨曦','喻梓林','张子宸','吴芯妍','郑若涵','丘梓言','曹楷瑞','赖子轩','李一蒽','张娟','谢启源','李佩芸','李长涯','莫悠','李昕蓉','张瓜瓜','黄政皓','欧阳文铭','彭宇德','张子轩','宋铭轩','刁国栋','吴敬轩','李璞','吴美萱','吴语桐','吴芷涵','谢富霖','曾利利','李宜静','吴羽萱','王思斯','宋希桐','张筱帆','江建宇','彭文韬','萧展鸣','洪曼佳','熊沐琰','詹一 一','柳翔睿','柳昱彤','徐若菡','谢昊言','廖奕沣','林楚桓','何紫瑜','张佳港','梁威','余嘉奇','林嘉琪','邱黄凡','莫子皓','陈梓昕','叶容媛','冯嘉怡','胡凯恩','廖凯伦','陈若琪','蓝天','何泽楷','吴泓毅','李欣','丁楷林','冯阳','王宥熙','洪嘉俊','蔡坤易','王子墨'];
    	$arr = [];
    	for ($i=0; $i < $num; $i++) { 
    		$arr[] = [
    			'clinic_id' => $clinicid, //病人的诊所id号
    			'name' => $nameArr[rand(0,count($nameArr)-1)], //姓名-真实姓名
    			'user_number' => 'P0' . date('yyMd') . rand(10000,99999), //病历号
    			'sex' => rand(0,2), //性别-微信性别1男2女0未知
    			'birthday' => time() - rand(5000,8000), //出生日期
    		];
    	}
    	$r = ClinicUser::insertAll($arr);
        echo 'OK';
    }

}

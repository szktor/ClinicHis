<?php
namespace app\index\controller\base;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use szktor\MyExcel; //excel 操作
use app\common\model\ClinicDictsheet; //字典表数据
use app\common\model\SystemDrug; //系统西药信息表
use think\facade\Url;
use think\facade\Env;
/*
* 前台-基础设置 - 数据导入
* index/base.import/index
*/
class Import extends BaseIndex
{
    protected $noNeedLogin = [];  //无需登录的方法，同时也就不需要鉴权了
    protected $noNeedRight = ['info','del','find_sysdrug']; //无需鉴权的方法，但需要登录

    //首页
    public function index()
    {
        if($this->request->isPost() || $this->request->isAjax())
        {
            
        }
        $tplArr = [
            //患者信息导入
            'hzxx' => ['excel_tpl_filename' => 'hzxx_tpl.xls', 'import_url' => Url::build('index/base.import/inport_hzxx')],
            //西药信息导入
            'xy' => ['excel_tpl_filename' => 'xy_tpl.xls', 'import_url' => Url::build('index/base.import/inport_xy')],
            //中药信息导入
            'zy' => ['excel_tpl_filename' => 'zy_tpl.xls', 'import_url' => Url::build('index/base.import/inport_zy')],
            //材料信息导入
            'cl' => ['excel_tpl_filename' => 'cl_tpl.xls', 'import_url' => Url::build('index/base.import/inport_cl')],
            //诊疗项目导入
            'zlxm' => ['excel_tpl_filename' => 'zlxm_tpl.xls', 'import_url' => Url::build('index/base.import/inport_zlxm')],
            //组合项目导入
            'zhxm' => ['excel_tpl_filename' => 'zhxm_tpl.xls', 'import_url' => Url::build('index/base.import/inport_zhxm')],
        ];
		$this->assign([
            'tpl_cfg' => json_encode($tplArr,JSON_UNESCAPED_UNICODE), //西药 - 药品类型
        ]);
        return $this->view->fetch();
    }
    //导入前统一处理
    private function loadExcelBefore()
    {
        try
        {
            $post = $this->request->only(['tag','path']);
            $post['path'] = isset($post['path']) ? urldecode($post['path']) : '';
            if(!isset($post['tag']) || $post['tag'] == '' || !$post['path'])
            {
                return json(['err' => '1', 'msg' => '对不起，参数有误']);
            }
            $excelFile = Env::get('root_path') . 'public' . $post['path'];
            if(!file_exists($excelFile))
            {
               return json(['err' => '2', 'msg' => '上传的文件不存在']);
            }
            //开始处理Excel
            $objPHPExcel = MyExcel::importFromExcel($excelFile);
            $excelArray = $objPHPExcel->getsheet(0)->toArray();   //转换为数组格式
            return $excelArray;
        }catch (\think\Exception $e){ // use think\Exception;
            return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }    
    }










    //患者信息导入
    public function inport_hzxx()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }

            _writeSystemLog($excelArray);





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //西药信息导入
    public function inport_xy()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //中药信息导入
    public function inport_zy()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //材料信息导入
    public function inport_cl()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //诊疗项目导入
    public function inport_zlxm()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
    //组合项目导入
    public function inport_zhxm()
    {
        if(!$this->request->isAjax())
        {
            return json(['err' => '1', 'msg' => '对不起，您的访问方式有误']);
        }
        try
        {
            //导入前统一处理
            $excelArray = $this->loadExcelBefore();
            array_shift($excelArray);  //删除第一个数组(大标题);
            array_shift($excelArray);  //删除第一个数组(小标题);
            if(!count($excelArray))
            {
                return json(['err' => '2', 'msg' => 'Excel文件中数据不存在']);
            }





            return json(['err' => '0', 'msg' => '数据已成功导入']);
        }catch (\think\Exception $e){ // use think\Exception;
           return json(['err' => '500', 'msg' => '错误：' . $e->getMessage()]);
        }
    }
}

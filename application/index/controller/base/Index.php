<?php
namespace app\index\controller\base;
use app\index\controller\BaseIndex;
use szktor\ClinicDict; //诊所门店公共字典
use app\common\model\ClinicDictsheet; //字典表
/*
* 前台-基础设置主页
*/
class Index extends BaseIndex
{
    public function index($tabname = '')
    {
		$this->assign([
            'tabname' => strtolower($tabname), //自动点击的tab
            'jcsz_all_list' => ClinicDict::getDictBaseConfigList(), //诊所设置大类列表
        ]);
        return $this->view->fetch();
    }

}

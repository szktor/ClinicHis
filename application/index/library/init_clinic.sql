-- 本文件注释 以 -- 开关，英文分号结束(每1分号表示为1条sql);
-- 诊所ID=CLINICID,管理员ID=ADMINID,管理员组ID=ADMINGPID;

-- 1.为该诊所创建一个分库;
CREATE DATABASE `zsdb_CLINICID` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- 2.为该分库创建一个表;
DROP TABLE IF EXISTS `zsdb_CLINICID` . `mxd_clinic_dictsheet`;
CREATE TABLE `zsdb_CLINICID` . `mxd_clinic_dictsheet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clinic_id` int(10) NOT NULL DEFAULT '0' COMMENT '诊所门店表的id',
  `type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `name` varchar(255) DEFAULT '' COMMENT '中文名称',
  `other_val` varchar(255) DEFAULT '' COMMENT '其它数值1',
  `other_val_two` varchar(255) DEFAULT '' COMMENT '其它数值2',
  `py_code` varchar(255) DEFAULT '' COMMENT '拼音码助记词',
  `wb_code` varchar(255) DEFAULT '' COMMENT '五笔码助记词',
  `sort` int(10) DEFAULT '0' COMMENT '排序',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态1.启用,0停用',
  `ext_json` longtext NOT NULL DEFAULT '' COMMENT '扩展json参数',
  `create_time` int(10) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='诊所门店字典表';



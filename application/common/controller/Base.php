<?php
namespace app\common\controller;
use think\Controller;
use think\facade\Cookie;
use think\facade\Session;
use think\facade\Url;
use app\common\model\Config as CfgModel;
use szktor\Random;

/*
* 所有控制器基类
*/
class Base extends Controller
{
    protected $site = []; //当前系统配置信息
    protected $ossConfig = []; //阿里云OSS配置
    protected $currRouteUrl = ''; //当前访问的url
	protected $pageNum = 20; //默认每页显示数量 

    //初始化
    public function initialize()
    {
        parent::initialize();
		set_time_limit(0);
		ini_set('memory_limit','-1');
		//移除HTML标签
		$this->request->filter('strip_tags');
		$modulename = $this->request->module(); //当前模块
		$controllername = strtolower($this->request->controller()); //当前控制器
		//pathinfo 方式获取 控制器名
		$pathArr = pathinfo($_SERVER['QUERY_STRING']);
		if(isset($pathArr['dirname']))
		{
		  $cName = strtolower(str_replace(['s=//index/','s=/index/','/index/'],'',trim($pathArr['dirname'])));         
		  if($cName && $cName != $controllername) $controllername = $cName;
		}
		$actionname = strtolower($this->request->action()); //当前控制器方法
		$path = str_replace('.', '/', $controllername) . '/' . $actionname;
		$this->currRouteUrl = strtolower('/' . $modulename . '/' . $path);  //当前URL路由 =  /模块/控制器/方法
		//获取配置参数信息
		$this->site = CfgModel::getWebConfig();
		header("Content-type: text/html; charset=" . $this->site['chartset']);
		date_default_timezone_set($this->site['timezone']);
		//阿里云OSS配置
        $this->ossConfig = [
            'up_alioss_onoff' => $this->site['up_alioss_onoff'],
            'accessKeyId' => $this->site['oss_accessKeyId'],
            'accessKeySecret' => $this->site['oss_accessKeySecret'],
            'endpoint' => $this->site['oss_endpoint'],
            'bucket' => $this->site['oss_bucket'],
        ];
        //客户端 uuid
        $tmpUuid = trim(Session::get($this->site['cookie_pre'] . 'public_uuid_str'));
        if(empty($tmpUuid))
        {
        	$tmpUuid = strtoupper(Random::uuid());
            Session::set($this->site['cookie_pre'] . 'public_uuid_str',$tmpUuid);
        }
        $this->site['client_uuid'] = $tmpUuid;

		$this->assign([
			'site' => $this->site,
		]);
    }


}

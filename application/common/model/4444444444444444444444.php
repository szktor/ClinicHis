<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use szktor\Tree;

//职位
class ClinicAdminZhiwei extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    /*
    * 获取职位列表 Select 选择器使用
    */
    public static function getZhiWeiList($clinic_id = 0, $userTree = true, $field = 'id,clinic_id,name,is_ywry,py_code,wb_code,sort,status')
    {
        $map = ['clinic_id' => $clinic_id, 'is_del' => '0'];
        $arr = self::where($map)->order('sort desc')->field($field)->select();
        if(count($arr) <= 0) return [];
        $arr = $arr->toArray();
        if($userTree)
        {
            $tree = Tree::instance();
            $tree->init($arr,null,' ');
            return $tree->getTreeList($tree->getTreeArray(0), 'name');
        }
        return $arr;
    }
	
    /*
    * 获取职位信息
    */
    public static function getZhiWeiInfo($id = 0,$clinic_id = 0, $field = 'id,clinic_id,name,is_ywry,py_code,wb_code,sort,status')
    {
        $arr = self::where(['clinic_id' => $clinic_id, 'id' => $id])->find();
        $arr = !$arr ? [] : $arr->toArray();
        return $arr;
    }
	
}

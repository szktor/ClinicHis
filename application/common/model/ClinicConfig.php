<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use app\common\model\Config;
use think\facade\Cache;
use think\facade\Request;

/**
 * 诊所门店配置模型
 */
class ClinicConfig extends ModelBasic
{
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    /*
    *获取诊所门店系统全局配置
    */
    public static function getClinicSystemConfig($clinic_id = 0, $_group = '')
    {
        $cacheName = 'cms_clinic_system_config_' . $clinic_id . '_' . $_group;
        $cfgArr = Cache::get($cacheName);
        if (!$cfgArr)
        {
            if($_group === '')
            {
               $cfgArr = self::getSplitDb($clinic_id,'clinic_config')->order('sort asc')->select();
            }else{
               $cfgArr = self::getSplitDb($clinic_id,'clinic_config')->where(['group' => trim($_group)])->order('sort asc')->select();
            }
            $tmpArr = [];
            if(count($cfgArr))
            {
              foreach ($cfgArr as $k => $v) {
                  $tmpArr[$v['name']] = $v['value'];
              }
            }
            $cfgArr = $tmpArr;
            Cache::set($cacheName, json_encode($cfgArr,JSON_UNESCAPED_UNICODE),3600);
        } else {
            $cfgArr = json_decode($cfgArr,true);
        }
        return $cfgArr;
    }

    /*
    *获取站点系统全局配置 -- 不缓存
    */
    public static function getBaseConfigArray($_group = '', $clinic_id = 0)
    {
        if(!$_group)
        {
           $cfgArr = self::getSplitDb($clinic_id,'clinic_config')->order('sort asc')->select();
        }else{
           $cfgArr = self::getSplitDb($clinic_id,'clinic_config')->where(['group' => trim($_group)])->order('sort asc')->select();
        }
        foreach ($cfgArr as $k => &$v) {
            $v['listarr'] = Config::getConfigValueArr(trim($v['content']));
        }
        return $cfgArr;
    }

    /*
    * 获取一个诊所门店的配置信息--指定字段
    */
    public static function getClinicConfigField($clinic_id = 0, $field = '')
    {
		$clinic_id = (int)$clinic_id;
		if($clinic_id <= 0 || !$field) return [];
		$cacheName = 'cms_websiteclinic_config_' . $clinic_id . '_' . $field;
		$info = Cache::get($cacheName);
		if ($info === '')
		{
			$info = self::getSplitDb($clinic_id,'clinic_config')->where(['name' => $field])->value('value');
			Cache::set($cacheName, $info ,3600);
		}
		return $info;
    }

}

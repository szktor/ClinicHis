<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use szktor\Tree;

class AuthRule extends ModelBasic
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    /*
    * 获取列表 Select 选择器使用
    */
    public static function getRuleList($module_type = '')
    {
		$map = [];
		if($module_type) $map[] = ['module_type','=',$module_type];
		$arr = self::where($map)->order('sort desc')->field('id,pid,title')->select()->toArray();
		$tree = Tree::instance();
		$tree->init($arr);
		return $tree->getTreeList($tree->getTreeArray(0), 'title');
    }
 
    /*
    * 获取设置角色组 权限下的 菜单
    */
    public static function getGroupSetRuleList($_tag = 'edit',$_group = '', $module_type = '')
    {
		$map = [];
		if($module_type) $map[] = ['module_type','=',$module_type];
		$arr = self::where($map)->order('sort desc')->field('id,pid,title,sort')->select()->toArray();
		$tree = Tree::instance();
		$tree->init($arr);
		if($_tag == 'add')
		{
			return $tree->getTreeArray(0);
		}else{
			return $tree->getGroupSetTreeArray(0,'',$_group); 
		}
    }

    /*
    * 获取设置角色组 权限下的 菜单 --- 前台设置
    */
    public static function getIndexGroupSetRuleList($_clinic = [], $_group = flase)
    {
    	if(!isset($_clinic['g_rules'])) return [];
    	$map = [
    		['status','=','1'],
    		['module_type','=','index_menu'],
    	];
    	if($_clinic['g_rules'] == '' || $_clinic['g_rules'] == '*')
    	{
    		//可使用所有功能


    	}else{
            $ids = array_unique(explode(',',trim($_clinic['g_rules'])));
            if(count($ids))
            {
            	$map[] = ['id','in',implode(',',$ids)];
            }
    	}
		$arr = self::where($map)->order('sort desc')->field('id,pid,title,sort')->select()->toArray();
		$tree = Tree::instance();
		$tree->init($arr);
		if(!$_group)
		{
			return $tree->getTreeArray(0);
		}else{
			return $tree->getGroupSetTreeArray(0,'',$_group); 
		}
    }
}

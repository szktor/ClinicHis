<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use app\common\model\Clinic;
use szktor\Random;

class ClinicAdmin extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $dateFormat = 'Y-m-d H:i';

    /*
    * 获取用户信息
    */
    public static function getClinicAdminList($clinic_id = 0, $where = [], $field = 'id,clinic_id,group_id,user_name,user_mobile,img,nickname')
    {
        $clinic_id = (int)$clinic_id;
        if($clinic_id <= 0) return [];
        $map = ['clinic_id' => $clinic_id, 'is_del' => '0', 'status' => '1'];
        if(is_array($where) && count($where))
        {
            $map = array_merge($map,$where);
        }
        $list = self::where($map)->field($field )->order('id desc')->select();
        if(!$list || count($list) <= 0) return [];
        foreach ($list as $key => &$val) {
            $nickName = isset($val['nickname']) ? $val['nickname'] : '';
            if(!$nickName) $nickName = isset($val['user_name']) ? $val['user_name'] : '';
            if(!$nickName) $nickName = isset($val['user_mobile']) ? $val['user_mobile'] : '';
            $val['nickname'] = $nickName;
        }
        return $list->toArray();
    }
    /*
    * 获取用户信息
    */
    public static function getAdminInfo($id = 0)
    {
		$id = (int)$id;
		if($id <= 0) return [];
		$map = ['a.id' => $id, 'a.is_del' => '0'];
		$info = self::alias('a')->join('clinic_auth_group b', 'a.clinic_id = b.clinic_id and a.group_id = b.id', 'left')
				->where($map)->field('a.*,b.name as g_name,b.rules as g_rules,b.status as g_status,b.remark as g_remark,b.index_page,b.is_del as g_is_del')
				->find();
		if(!$info || !isset($info['id']) || intval($info['id']) <= 0) return [];
        $nickName = isset($info['nickname']) ? $info['nickname'] : '';
        if(!$nickName) $nickName = isset($info['user_name']) ? $info['user_name'] : '';
        if(!$nickName) $nickName = isset($info['user_mobile']) ? $info['user_mobile'] : '';
        $info['nickname'] = $nickName;
		return $info->toArray();
    }
    /*
    * 获取用户信息--登录
    */
    public static function getAdminLoginInfo($id = 0, $pwd = '')
    {
		$id = (int)$id;
		if($id <= 0) return false;
		$map = ['a.id' => $id, 'a.status' => '1', 'a.is_del' => '0'];
		$info = self::alias('a')->join('clinic_auth_group b', 'a.clinic_id = b.clinic_id and a.group_id = b.id', 'left')
				->where($map)->field('a.*,b.name as g_name,b.rules as g_rules,b.status as g_status,b.remark as g_remark,b.index_page,b.is_del as g_is_del')
				->find();
		if(!$info || !isset($info['id']) || intval($info['id']) <= 0) return false;
		if($pwd && $info['user_pwd'] != $pwd) return false; //已修改密码，需要重新登录
		if(!$info['group_id']) return false; //未配置分组
		if(!$info['g_status']) return false; //分组已禁用
		if(intval($info['g_is_del']) > 0) return false; //分组已被删除
        $info = $info->toArray();
        $clinicInfo = Clinic::getClinicInfo($info['clinic_id']);
        $checkRes = _getCurrAdminRules($info,$clinicInfo);
        $info['clinic_info'] = $clinicInfo;
        $info['rules_check_res'] = $checkRes;
		return $info;
    }
    /**
     * 重置用户密码
     */
    public static function resetPassword($uid, $NewPassword, $salt = '')
    {
        $passwd = self::encryptPassword($NewPassword,$salt);
        $ret = self::where(['id' => $uid])->update(['user_pwd' => $passwd, 'pwd_salt' => $salt]);
        return $ret;
    }
    /*
     * 密码加密
    */
    public static function encryptPassword($password, $salt = '', $encrypt = 'md5')
    {
        return $encrypt($password . $salt);
    }
	
    /**
    * 生成唯一字段值 唯一
    */
    public static function buildUniqueFieldsVal($len = 5, $fields = 'user_name')
    {
		$isExists = true;
		$tmpCode = '';
		do {
			$tmpCode = strtoupper(Random::alpha($len) . Random::nozero($len)); //数字和字母
			$isExists = self::checkUniqueCode(new ClinicAdmin(),$tmpCode,$fields);
		} while ($isExists);
		return $tmpCode;
    }
    
	
}

<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use think\facade\Cache;
use think\facade\Request;
use app\common\model\ClinicGroup;

/**
 * 配置模型
 */
class Config extends ModelBasic
{

    // 表名,不含前缀
    protected $name = 'config';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    // 追加属性
    protected $append = [];

    /*
    *获取站点系统全局配置
    */
    public static function getWebConfig($_group = '')
    {
        $cfgArr = Cache::get('cms_website_config' . $_group);
        if (!$cfgArr)
        {
            if(!$_group)
            {
               $cfgArr = self::order('sort asc')->select();
            }else{
               $cfgArr = self::where(['group' => trim($_group)])->order('sort asc')->select();
            }
            $tmpArr = [];
            if(count($cfgArr))
            {
              foreach ($cfgArr as $k => $v) {
                  $tmpArr[$v['name']] = $v['value'];
              }
            }
            $cfgArr = $tmpArr;
            Cache::set('cms_website_config' . $_group, serialize($cfgArr),3600);
        } else {
            $cfgArr = unserialize($cfgArr);
        }
        $cfgArr['version'] = date("YmdHis", time());  //css js 资源文件版本
        $cfgArr['year_str'] = date("Y", time());
		if(isset($cfgArr['cdnurl']) && isset($cfgArr['weburl']))
		{
			$cfgArr['static_path'] = !$cfgArr['cdnurl'] ? $cfgArr['weburl'] . '/static/' : $cfgArr['cdnurl'] . '/static/'; //css js 资源文件路径
		}
        return $cfgArr;
    }

    /*
    *获取站点系统全局配置 -- 不缓存
    */
    public static function getWebConfigArray($_group = '')
    {
        if(!$_group)
        {
           $cfgArr = self::order('sort asc')->select();
        }else{
           $cfgArr = self::where(['group' => trim($_group)])->order('sort asc')->select();
        }
        foreach ($cfgArr as $k => &$v) {
            $v['listarr'] = self::getConfigValueArr(trim($v['content']));
            //注册或开户默认的商家组
            if($v['name'] == 'reg_default_groupid')
            {
                $tempList = ClinicGroup::getGroupList(false);
                $tempSelectArr = [
                    ['name' => '关闭注册', 'val' => '0']
                ];
                if(count($tempList) > 0)
                {
                    foreach ($tempList as $ka => $va) {
                        $tempSelectArr[] = ['name' => trim($va['name']), 'val' => trim($va['id'])];
                    }
                    $v['listarr'] = $tempSelectArr;
                }
            }
        }
        return $cfgArr;
    }

    /*
    *获取站点系统全局配置 -- 变量字典数据
    */
    public static function getConfigValueArr($_content = '')
    {
      if(!$_content)
      {
         return '';
      }
      $arr = explode('#', $_content);
      $list = '';
      foreach($arr as $v)
      {
        if($v)
        {
          $_arr = explode('|', $v);
          if(isset($_arr[0]) && isset($_arr[1]))
          {
            $list[] = ['name' => trim($_arr[0]), 'val' => trim($_arr[1])];
          }
        }
      }
      return $list;
    }

}

<?php

namespace app\common\model;

use app\common\model\ModelBasic;

class Admin extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;  //update_time
    protected $dateFormat = false;

    /*
    * 获取用户信息
    */
    public static function getAdminInfo($id = 0)
    {
		$id = (int)$id;
		if($id <= 0) return [];
		$map = ['a.id' => $id, 'a.is_del' => '0'];
		$info = self::alias('a')->join('auth_group b', 'a.group_id = b.id', 'left')
				->where($map)->field('a.*,b.name as g_name,b.rules as g_rules,b.status as g_status,b.remark as g_remark,b.index_page')
				->find();
		if(!$info || !isset($info['id']) || intval($info['id']) <= 0) return [];
		return $info->toArray();
    }
    /*
    * 获取用户信息--登录
    */
    public static function getAdminLoginInfo($id = 0, $pwd = '')
    {
		$id = (int)$id;
		if($id <= 0) return false;
		$map = ['a.id' => $id, 'a.status' => '1', 'a.is_del' => '0'];
		$info = self::alias('a')->join('auth_group b', 'a.group_id = b.id', 'left')
				->where($map)->field('a.*,b.name as g_name,b.rules as g_rules,b.status as g_status,b.remark as g_remark,b.index_page')
				->find();
		if(!$info || !isset($info['id']) || intval($info['id']) <= 0) return false;
		if($pwd && $info['admin_pwd'] != $pwd) return false; //已修改密码，需要重新登录
		if(!$info['group_id']) return false; //未配置分组
		if(!$info['g_status']) return false; //分组已禁用
		return $info->toArray();
    }
    /**
     * 重置用户密码
     */
    public static function resetPassword($uid, $NewPassword)
    {
        $passwd = self::encryptPassword($NewPassword);
        $ret = self::where(['id' => $uid])->update(['admin_pwd' => $passwd]);
        return $ret;
    }
    /*
     * 密码加密
    */
    public static function encryptPassword($password, $salt = '', $encrypt = 'md5')
    {
        return $encrypt($password . $salt);
    }
}

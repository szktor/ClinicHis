<?php
namespace app\common\model;

use app\common\model\ModelBasic;
use think\facade\Config as tpConfig;
use think\Db;
use think\facade\Cache;
use szktor\Random;

class Clinic extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;  //update_time
    protected $dateFormat = false;

    /*
    * 获取诊所列表 $id = 诊所 id
    */
    public static function getSelectClinicList($id = '', $field = 'a.id,a.mall_id,a.group_id,a.long_name,a.short_name,a.shop_code,a.img_logo,a.status')
    {
		$map = [['a.is_del' => '0']];
		if($id) $map[] = ['a.id','IN',$id];
		$newFields = 'b.name as g_name,b.rules as g_rules,b.status as g_status';
		$newFields .= !$field ? ',a.*' : ','.$field;
		$arrA = self::alias('a')->join('clinic_group b', 'a.group_id = b.id', 'left')
		        ->where($map)->field($newFields)->order('sort desc')->select();
		if(count($arrA) <= 0)
		{
			return [];
		}
		return $arrA->toArray();
    }
    /*
    * 获取诊所信息 $id = 诊所 id
    */
    public static function getClinicInfo($id = 0, $field = 'a.id,a.mall_id,a.group_id,a.long_name,a.short_name,a.shop_code,a.img_logo,a.status')
    {
		$cacheName = 'cms_websiteclinic_info_' . $id;
        $info = Cache::get($cacheName);
		if (!$info)
		{
			$map = [['a.id','=',$id],['a.is_del','=','0']];
			$newFields = 'b.name as g_name,b.rules as g_rules,b.status as g_status';
			$newFields .= !$field ? ',a.*' : ','.$field;
			$arrA = self::alias('a')->join('clinic_group b', 'a.group_id = b.id', 'left')->where($map)->field($newFields)->order('a.id desc')->find();
			if(!$arrA || !isset($arrA['id']) || intval($arrA['id']) <= 0)
			{
				return [];
			}
			$info = $arrA->toArray();
			Cache::set($cacheName, json_encode($info,JSON_UNESCAPED_UNICODE) ,3600);
		}else{
			$info = json_decode($info,true);
		}
		return $info;
    }
    /*
    * 验证一个门店/总店编码是否有效
	* return false / array
    */
    public static function verifyShopCode($code = '', $isMall = false)
    {
		if(!$code) return false;
		$code = strtoupper($code);
		$map = [['a.shop_code','=',$code],['a.is_del','=','0']];
		$newFields = 'a.*,b.name as g_name,b.rules as g_rules,b.status as g_status';
        $arrA = self::alias('a')->join('clinic_group b', 'a.group_id = b.id', 'left')->where($map)
		        ->field($newFields)->order('a.id desc')->find();
        if(!$arrA || !isset($arrA['id']) || intval($arrA['id']) <= 0)  return false;
		$arrA = $arrA->toArray();
		if($isMall)
		{
			if(intval($arrA['mall_id']) > 0) return false; //这是一个分店
		}
		//已过期
        if($arrA['except_time'] > 0 && time() > $arrA['except_time']) return false;
		return $arrA;
    }
    /**
    *获取诊所名字 $id = 诊所 id
    */
    public static function getClinicName($id = 0,$short = false)
    {
		if(!$short)
		{
			$_name = self::where(['id' => $id])->value('long_name');
		}else{
			$_name = self::where(['id' => $id])->value('short_name');
		}
		return $_name ? $_name : '未知';
    }
    /**
    *生成唯一字段值 唯一
    */
    public static function buildUniqueFieldsVal($len = 8, $fields = 'shop_code')
    {
		$isExists = true;
		$tmpCode = '';
		do {
			$tmpCode = strtoupper(Random::alnum($len)); //数字和字母
			$isExists = self::checkUniqueCode(new Clinic(),$tmpCode,$fields);
		} while ($isExists);
		return $tmpCode;
    }

}



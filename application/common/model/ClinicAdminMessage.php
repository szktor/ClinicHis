<?php

namespace app\common\model;

use app\common\model\ModelBasic;

class ClinicAdminMessage extends ModelBasic
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'read_time';
    protected $dateFormat = 'Y-m-d H:i:s';
    /*
    * 获取一条消息内容
    */
    public static function getOneInfo($id = 0,$admin_id = 0)
    {
        $id = (int)$id;
        if($id <= 0 || (int)$admin_id <= 0) return [];
        $map = ['id' => $id, 'admin_id' => (int)$admin_id];
        $info = self::where($map)->find();
        if(!$info || !isset($info['id']) || intval($info['id']) <= 0) return [];
        return $info->toArray();
    }
    /*
    * 设置消息未读、已读状态
    */
    public static function setMessageStatus($admin_id = 0, $id = 0, $status = 0)
    {
        if(!$admin_id) return false;
        $map = [['admin_id','=',$admin_id]];
        if($id != '')
        {
            $map[] = ['id','in',$id];
        }
        if((int)$status > 0)
        {
           $now_time = time();
           return self::where($map)->update(['status' => '1', 'read_time' => $now_time]);
        }else{
           return self::where($map)->update(['status' => '0', 'read_time' => '0']);
        }
    }

    /*
    * 获取消息未读、已读 数量
    * 消息类型system=系统,notify=通知,private=私信
    */
    public static function getMessageCount($admin_id = 0, $msgtype = '', $status = '')
    {
        if(!$admin_id) return 0;
        $map = [['admin_id','=',$admin_id]];
        if($msgtype) $map[] = ['msg_type','=',$msgtype];
        if($status != '')
        {
            $status = (int)$status > 0 ? 1 : 0;
            $map[] = ['status','=',$status];
        }
        return self::where($map)->count();
    }

}

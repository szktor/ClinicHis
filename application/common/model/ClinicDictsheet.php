<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use app\common\model\Clinic;
use app\common\model\Config;
use think\facade\Cache;
use szktor\Tree;

//分库中的表模型方法
class ClinicDictsheet extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $dateFormat = false;

    /*
    * 获取字典数据列表 Select 选择器使用
    */
    public static function getDictList($type = '', $clinic_id = 0, $field = '*')
    {
        $map = [
            ['status','=','1'],
            ['clinic_id','=',$clinic_id]
        ];
        if($type) $map[] = ['type', '=', strtolower($type)];
        $arr = self::getSplitDb($clinic_id,'clinic_dictsheet')
               ->where($map)->order('sort desc')->field($field)->select();
        if(count($arr) <= 0) return [];
        foreach ($arr as $key => &$val) {
            if(isset($val['ext_json']))
            {
                $val['ext_json'] = !$val['ext_json'] ? [] : json_decode($val['ext_json'],true);
            }
        }
        return $arr;
    }
    /*
    * 获取科室列表 Select 选择器使用
    */
    public static function getClinicKeShiList($clinic_id = 0, $userTree = true, $field = 'id,pid,name', $where = [])
    {
        $map = ['clinic_id' => $clinic_id];
        if($where && count($where) > 0) $map = array_merge($map,$where);
        $arr = self::getSplitDb($clinic_id,'clinic_keshi')
               ->where($map)->order('id desc')->field($field)->select();
        if(count($arr) <= 0) return [];
        if($userTree)
        {
            $tree = Tree::instance();
            $tree->init($arr,null,' ');
            return $tree->getTreeList($tree->getTreeArray(0), 'name');
        }
        return $arr;
    }
    /*
    * 获取科室信息
    */
    public static function getClinicKeShiInfo($id = 0, $clinic_id = 0, $field = 'id,pid,name')
    {
        $map = ['clinic_id' => $clinic_id, 'id' => $id];
        $arr = self::getSplitDb($clinic_id,'clinic_keshi')
               ->where($map)->field($field)->find();
        if(!$arr) return [];
        return $arr;
    }


}

<?php

namespace app\common\model;

use app\common\model\ModelBasic;

//系统西药信息表
class SystemDrug extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $dateFormat = 'Y-m-d H:i:s';

}

<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use szktor\Tree;

class ClinicGroup extends ModelBasic
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $dateFormat = 'Y-m-d H:i:s';

    /*
    * 获取角色组列表 Select 选择器使用
    */
    public static function getGroupList($userTree = true, $field = 'id,pid,name,except_str,remark')
    {
        $map = ['is_del' => '0'];
        $arr = self::where($map)->order('sort desc')->field($field)->select();
        if(count($arr) <= 0) return [];
        $arr = $arr->toArray();
        if($userTree)
        {
            $tree = Tree::instance();
            $tree->init($arr);
            return $tree->getTreeList($tree->getTreeArray(0), 'name');
        }
        return $arr;
    }
    /*
    * 获取角色组信息
    */
    public static function getGroupInfo($id = 0, $field = '*')
    {
        $arr = self::where(['id' => $id])->field($field)->find();
        $arr = !$arr ? [] : $arr->toArray();
        return $arr;
    }

}

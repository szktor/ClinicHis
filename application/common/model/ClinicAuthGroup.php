<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use szktor\Tree;

class ClinicAuthGroup extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $dateFormat = 'Y-m-d H:i';

    /*
    * 获取角色组列表 Select 选择器使用
    */
    public static function getGroupList($clinic_id = 0, $userTree = true, $field = 'id,pid,clinic_id,name,status,remark,index_page')
    {
        $map = ['clinic_id' => $clinic_id, 'is_del' => '0'];
        $arr = self::where($map)->order('id desc')->field($field)->select();
        if(count($arr) <= 0) return [];
        $arr = $arr->toArray();
        if($userTree)
        {
            $tree = Tree::instance();
            $tree->init($arr,null,' ');
            return $tree->getTreeList($tree->getTreeArray(0), 'name');
        }
        return $arr;
    }
    /*
    * 获取角色信息
    */
    public static function getGroupInfo($id = 0,$clinic_id = 0)
    {
        $arr = self::where(['clinic_id' => $clinic_id, 'id' => $id])->find();
        $arr = !$arr ? [] : $arr->toArray();
        if(count($arr))
        {
           $arr['index_page'] = !$arr['index_page'] ? '' : strtolower(trim($arr['index_page']));
        }
        return $arr;
    }
    /*
    * 获取角色组名
    */
    public static function getGroupName($id = 0,$clinic_id = 0)
    {
        $name = self::where(['clinic_id' => $clinic_id, 'id' => $id])->order('id desc')->value('name');
        return !$name ? '未知' : $name;
    }


}

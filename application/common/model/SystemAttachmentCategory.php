<?php

namespace app\common\model;

use app\common\model\ModelBasic;

class SystemAttachmentCategory extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = false;
    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $dateFormat = false;


}

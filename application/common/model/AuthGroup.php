<?php

namespace app\common\model;

use app\common\model\ModelBasic;
use szktor\Tree;

class AuthGroup extends ModelBasic
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $dateFormat = 'Y-m-d H:i:s';

    /*
    * 获取角色组列表 Select 选择器使用
    */
    public static function getGroupList()
    {
      $arr = self::order('id desc')->field('id,pid,name')->select()->toArray();
      $tree = Tree::instance();
      $tree->init($arr);
      return $tree->getTreeList($tree->getTreeArray(0), 'name');
    }
    /*
    * 获取角色信息
    */
    public static function getGroupInfo($id = 0)
    {
        $arr = self::where(['id' => $id])->find();
        $arr = !$arr ? [] : $arr->toArray();
        if(count($arr))
        {
           $arr['index_page'] = !$arr['index_page'] ? '' : strtolower(trim($arr['index_page']));
        }
        return $arr;
    }
    /*
    * 获取角色组名
    */
    public static function getGroupName($id = 0)
    {
        $name = self::where(['id' => $id])->order('id desc')->value('name');
        return !$name ? '未知' : $name;
    }


}

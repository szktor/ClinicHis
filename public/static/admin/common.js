var _Admin = null, layer = null, Laypage = null, laydate = null, _ajaxRun = false, _audio = null;
$(document).ready(function(){

});
$(function(){
  layui.use(['laypage','layer','laydate'], function(){
  	layer = layui.layer;
  	Laypage = layui.laypage;
  	laydate = layui.laydate;
  });
});
//Ajax post 请求
var _Admin = {
	ajax:function(url,param,callback,txt)
	{
     if(_ajaxRun) return false;
     txt = txt||'';
	 $.ajax({
	      url: url,
	      dataType:"json",
	      data:param,
	      type:'POST',
	      async:true,
	      beforeSend:function(){
		    _ajaxRun = true;
			// if(txt != '')
			// {
			//   layer.msg(txt, {icon: 16,time:50000,shade:[0.4,'#000000']});
			// }else{
			//   layer.load(1, {time:50000,shade:[0.4,'#000000']}); //0代表加载的风格，支持0-2
			// }
			_showLoading();
	      },
	      success:function(d){
	        _ajaxRun = false;
	        _hideLoading();
		    layer.closeAll();
	        callback(d);
	      },
	      error: function(){
		    _ajaxRun = false;
		    _hideLoading();
	        layer.closeAll();
	      }
	  });
	},
	//清除系统缓存
    wipecache:function(url){
    	_showLoading();
    	//layer.load(1, {shade:[0.4,'#000000']});
        $.get(url, function(d){
		    _hideLoading();
		    layer.closeAll();
		    layer.alert(d.msg);
		});
    },
    //播放音频
    playAudio:function(src,k){
      k = k || 0;
      var obj = $('#html_autio_' + k);
      if(obj.length > 0) obj.remove();
      $('body').append('<audio controls="controls" style="display:none" id="html_autio_'+k+'" autoplay><source src="' + src + '"/></audio>');
      /*
      k = k || 0;
      var obj = $('#html_autio_' + k);
      if(obj.length > 0)
      {
        obj[0].play();
      }else{
      	$('body').append('<audio controls="controls" style="display:none" id="html_autio_'+k+'" autoplay><source src="' + src + '"/></audio>');
      }
      */
    },
    //excel导出
    downExcel:function(obj){
	  layer.confirm('导出需要一些时间，确定要导出吗？', {
	      title:'导出Excel',btn: ['确定','取消'],
	      btn1:function(){
	        layer.closeAll();
	        $(obj).submit();
	      }
	  });
    },
    //将页面Table数据导出为Excel
    saveTableToExcel:function(_ele,_name){
	  layer.confirm('导出需要一些时间，确定要导出吗？', {
	      title:'导出Excel',btn: ['确定','取消'],
	      btn1:function(){
	      	layer.closeAll();
	        _name = _name || new Date().getTime()+'_excel.xls';
            $.table2excel({ selector: $(_ele),filename:_name });
	      }
	  });
    },
    //空数据
    emptyTabel:function(_msg,_col,_obja,_objb){
		$(_obja).empty();
		$(_obja).append('<tr class="gradeC"><td colspan="' + _col + '" style="padding:30px 0px;color:#ff0000;text-align:center;">' + _msg + '</td></tr>');
		$(_objb).hide();
    },
    //对于input框只能输入数字和小数点
    bindnumber:function(obj){
	  $(obj).bind("keyup",function(){
	     $(this).val($(this).val().replace(/[^\-?\d.]/g,''));
	  });
	  $(obj).focus(function(){$(this).select();});	
    },
    //input 只能输入整数
    bindInputNum:function(obj,_val){
	  $(obj).bind("keyup",function(){
	    $(this).val($(this).val().replace(/[^\-?\d]/g,_val));
	    if(parseInt($(this).val()) <= 0)
	    {
	      $(this).val(_val);
	      layer.msg('请输入整数数值', {time: 1000}); 
	    }
	  });
	  $(obj).focus(function(){$(this).select();}); 
    },
    //input 只能输入整数 - 不能超最最大值
    bindInputNumTwo:function(obj,_val){
	  $(obj).bind("keyup",function(){
	    $(this).val($(this).val().replace(/[^\-?\d]/g,_val));
	    if(parseInt($(this).val()) > _val || parseInt($(this).val()) <= 0)
	    {
	      $(this).val(_val);
	      layer.msg('请输入整数数值', {time: 1000}); 
	    }
	  });
	  $(obj).focus(function(){$(this).select();});
    },
    //input 只能输入整数 - 不提示
    bindInputNumThree:function(obj,_val){
	  $(obj).bind("keyup",function(){
	    $(this).val($(this).val().replace(/[^\-?\d]/g,_val));
	  });
	  $(obj).focus(function(){$(this).select();}); 
    },
    //只能输入价格
	bindMoney:function(obj,_val){
	 $(obj).bind("keyup",function(){
	    if($(this).val() != '0.' && $(this).val() != '0.0')
	    {
	      $(this).val($(this).val().replace(/[^\-?\d.]/g,_val));
	      if(parseFloat($(this).val()) <= 0)
	      {
	        $(this).val(_val);
	        layer.msg('请输入数值', {time: 1000}); 
	      }
	    }
	 });
	 $(obj).focus(function(){$(this).select();}); 
	},
    //绑定回车事件
    bindEnter:function(_obj,fun)
    {
		$(_obj).bind('keypress',function(event){
		   if(event.keyCode == "13")
		   {
		      if(typeof(fun) == 'function') fun();
		   }
		});
    },
    //审核事件查看 {title:'',type:'qh',id:'0',ordersn:''}
    showWindowForShenHeLog:function(_obj)
    {
      if(typeof(_obj) != 'object')
      {
      	layer.msg('参数错误，需要Object参数', {time: 1500});
      	return false;
      }
      var param = '',_title = '审核日志查看';
      if(typeof(_obj.id) != 'undefined' && parseInt(_obj.id) > 0) param += '&sqlid=' + parseInt(_obj.id);
      if(typeof(_obj.ordersn) != 'undefined' && _obj.ordersn != '') param += '&sn=' + _obj.ordersn;
      if(typeof(_obj.title) != 'undefined' && _obj.title != '') _title = _obj.title;
	  layer.open({
	    title:_title,
	    type: 2,
	    area: ['1000px', '600px'],
	    content: "/admin/index/view_shenhe_log.html?type=" + _obj.type + param,
	  });
    },
    //Excel 导入未成功的 显示出 alert 列表
    buildFailGoodsIdHtml:function(_list,_txt,_split)
    {
    	_txt = _txt || '商品ID为';
        _split = _split || '<br/>';
	    if(Object.keys(_list).length <= 0) return '';
	    var _html = '';
	    $.each(_list,function(i,v){
	      _html += _split + parseInt(i+1) + '：' + _txt + '：' + v;
	    });
	    return _html;
    },
    //门店相关二维码
    ShowQrcodeDialog:function(_url,_shopid)
    {
		layer.open({
	        title:'门店相关二维码',
	        type: 2,
	        area: ['850px', '600px'],
	        content: _url + "?shopid=" + _shopid
	    });
    }

};

//显示Ajax加载层
function _showLoading()
{
	if($('#ajaxMaskLoad').length > 0)
	{
	  $('#ajaxMaskLoad').remove();
	}
	var _html = '<div id="ajaxMaskLoad" style="z-index:99999999999999;">\
	                 <div class="spinner">\
	                     <div class="bounce1"></div>\
	                     <div class="bounce2"></div>\
	                     <div class="bounce3"></div>\
	                 </div>\
	            </div>';
	$('body').append(_html);
}
//隐藏Ajax加载层
function _hideLoading()
{
	if($('#ajaxMaskLoad').length > 0)
	{
	  $('#ajaxMaskLoad').hide();
	  $('#ajaxMaskLoad').remove();
	}
	$('#ajaxMaskLoad').hide();
	$('#ajaxMaskLoad').remove();
}
/**
 * 图片显示失败时
 */
function imgerrorfun() {
    var img = event.srcElement;
    img.src = "/static/icon_nopic.png";
    img.onerror = null;
}
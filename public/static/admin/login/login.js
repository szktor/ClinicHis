$(function(){
	$("body").keypress(function(event){
	   if(event.which === 13){
       $('#loginFormBtn').trigger('click');
     }
	});
	$('#mName').focus();
	zylVerCode(); //生成验证码
	//点击刷新验证码
	$('.captcha-img').click(function(){ zylVerCode(); });
});
//生成验证码
function zylVerCode(){
    $('.captcha-img img').attr('src',yzmUrl + '&t=' + Math.random());
    $('#vercode').val('');
}
function _loginUser()
{
 var name = $.trim($('#mName').val());
 var pwd = $.trim($('#mPwd').val());
 var _yzm = $.trim($('#vercode').val());
 if(name.length <= 0 || name == '')
 {
   layer.msg('请输入登录账号',{anim: 4});
   //layer.tips('请输入登录账号', '#mName',{tips:[2, '#ff0000']});
   $('#mName').focus();
   return false;
 }
 if(pwd.length <= 0 || pwd == '')
 {
   layer.msg('请输入登录密码',{anim: 4});
   //layer.tips('请输入登录密码', '#mPwd',{tips:[2, '#ff0000']});
   $('#mPwd').focus();
   return false;
 }
 if(_yzm.length <= 0 || _yzm == '')
 {
   layer.msg('请输入图片验证码',{anim: 4});
   //layer.tips('请输入图片验证码', '#vercode',{tips:[2, '#ff0000']});
   $('#vercode').focus();
   return false;
 }
 _Admin.ajax(loginUrl,{captcha_key:_captchaKey,name:name,pwd:pwd,yzm:_yzm},function(d){
	zylVerCode(); //生成验证码
    if(d.err == '0')
    {
      layer.msg(d.msg,{time:500},function(){
         window.location.href = _returnUrl;
      });
    }else{
      layer.msg(d.msg,{anim: 6});
    }
 });
}


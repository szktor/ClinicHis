if(window.frames.length != parent.frames.length)
{ 
    window.top.location.href = window.location.href;
}
$(function(){
	var num=parseInt(Math.random()*10000)%3;
	$("body").css("background","url("+logCfg.staticPath+"bg"+num+".jpg) center top / cover no-repeat");
	$(".tabs").click(function(){
		var index=$(this).index();
		logCfg._loginMode = index;
		if(index == 0)
		{
			$('#utel').val('');
			$('#captcha_code').val('');
			$('#phone_code').val('');
			$('#uname').focus();
		}
		if(index == 1)
		{
			$('#uname').val('');
			$('#upwd').val('');
			$('#utel').focus();
			//重新 设置验证码
			_setNewCaptcha();
		}
		if(index == 2)
		{
			$('#uname').val('');
			$('#upwd').val('');
			$('#utel').val('');
			$('#captcha_code').val('');
			$('#phone_code').val('');
		}
		$(".tabs").removeClass("active").eq(index).addClass("active");
		$(".content").hide().eq(index).show()
	});
	$(".ico").hover(function(){
		$(".info").fadeIn(500)
	},function(){
		$(".info").fadeOut(500)
	});
	$("body").mousemove(function(e){
		var W=$("body").width();
		var H=$("body").height();
		var x=60-e.clientX*60/W;
		var y=60-e.clientY*60/H;
		$(".ad").animate({left:x,top:y},10)
	});
	//点击刷新验证码
	$('.captcha-img').click(function(){ _setNewCaptcha(); });
	//点击发送验证码
	$('.tel-codebtn').click(function(){ _setSendCode(); });
    //登录点击
    $('.loginBtn1,.loginBtn2').click(function(){ _loginAction(); });
	$("body").keypress(function(event){
	    if(event.which === 13){
	        _loginAction();
	    }
	});
});
//设置验证码
function _setNewCaptcha()
{
	$('.captcha-img img').attr('src',logCfg._captchaUrl + '&t=' + Math.random());
	$('#captcha_code').val('');
}
//向手机号发送验证码
function _setSendCode()
{
	var _issend = $('.tel-codebtn').data('issend');
	if(_issend == true)
	{
		layer.msg('验证码已发送，请注意查收', {time: 2000,anim: 4});
		return false;
	}
	var param = {
		captcha_key:logCfg._captchaKey,
		tel:$.trim($('#utel').val()),
		captcha:$.trim($('#captcha_code').val()),
		code:$.trim($('#phone_code').val()),
	};
	if(param.tel == '' || param.tel.length <= 0)
	{
		layer.msg('请输入手机号码', {time: 2000,anim: 4});
		return false;
	}
	if(!_Index.checkPhone(param.tel))
	{
		layer.msg('您输入的手机号码格式不正确', {time: 2000,anim: 4});
		return false;
	}
	if(param.captcha == '' || param.captcha.length <= 0)
	{
		layer.msg('请输入图片验证码', {time: 2000,anim: 4});
		return false;
	}
	_Index.ajax(logCfg.sendCodeUrl,param,function(d){
		if(d.err == 0)
		{
			$('.tel-codebtn').data('issend','true');
			layer.msg(d.msg, {time: 2500});
			logCfg._sTimer = setInterval(function(){
				if(parseInt(logCfg._t) < 0)
				{
					logCfg._t = 60;
					//重新 设置验证码
					_setNewCaptcha();
					$('.tel-codebtn').data('issend','false');
					$('.tel-codebtn').html('点击发送验证码');
					clearInterval(logCfg._sTimer);
				}else{
					$('.tel-codebtn').html(logCfg._t + ' 秒后可重发');
					logCfg._t--;
				}
			},1000);
			//重新 设置验证码
			_setNewCaptcha();
		}else{
			//重新 设置验证码
			_setNewCaptcha();
			layer.msg(d.msg, {time: 2000,anim: 4});
		}
	},'正在发送');
}
//登录事件
function _loginAction()
{
    var param = {
		back_url:logCfg.returnUrl,
		loginmode:logCfg._loginMode,
		name:$.trim($('#uname').val()),
		pwd:$.trim($('#upwd').val()),
		tel:$.trim($('#utel').val()),
		captcha:$.trim($('#captcha_code').val()),
		captcha_key:logCfg._captchaKey,
		code:$.trim($('#phone_code').val()),
	};
	if(parseInt(param.loginmode) == 2) //扫码
	{
		layer.msg('请选择正确的登录方式', {time: 2000,anim: 4});
		return false;
	}
	if(parseInt(param.loginmode) == 0) //账号
	{
		if(param.name == '' || param.name.length <= 0)
		{
			layer.msg('请输入登录账号', {time: 2000,anim: 4});
			return false;
		}
		if(param.name.length <= 6)
		{
			layer.msg('登录账号长度格式不正确', {time: 2000,anim: 4});
			return false;
		}
		if(param.pwd == '' || param.pwd.length <= 0)
		{
			layer.msg('请输入登录密码', {time: 2000,anim: 4});
			return false;
		}
		if(param.pwd.length <= 6)
		{
			layer.msg('登录密码长度格式不正确', {time: 2000,anim: 4});
			return false;
		}
	}
	if(parseInt(param.loginmode) == 1) //手机号-注册+登录
	{
		if(param.tel == '' || param.tel.length <= 0)
		{
			layer.msg('请输入手机号码', {time: 2000,anim: 4});
			return false;
		}
		if(!_Index.checkPhone(param.tel))
		{
			layer.msg('您输入的手机号码格式不正确', {time: 2000,anim: 4});
			return false;
		}
		if(param.code == '' || param.code.length <= 0)
		{
			layer.msg('请输入手机验证码', {time: 2000,anim: 4});
			return false;
		}
	}
	_Index.ajax(logCfg.loginUrl,param,function(d){
		if(d.err == 0)
		{
			$('#uname').val('');
			$('#upwd').val('');
			$('#utel').val('');
			$('#captcha_code').val('');
			$('#phone_code').val('');
			layer.msg(d.msg, {time: 1000},function(){ window.location.href = d.url; });
		}else{
			if(parseInt(param.loginmode) == 1) //手机号-注册+登录
			{
				//重新 设置验证码
				_setNewCaptcha();
			}
			layer.msg(d.msg, {time: 2000,anim: 4});
		}
	},'请稍候...');
}
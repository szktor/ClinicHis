$(function(){
  
});
//共用字典设置
function _publicSetDialog(objData)
{
    var jsonData = objData.json || '',_title = _dictText;
    if($.isEmptyObject(jsonData))
    {
       _title = _title + ' 添加';
    }else{
       _title = _title + ' 修改';
    }
    objData.txt = objData.txt || '输入名称';
    objData.top = objData.top || 0;
    _Index.tplDlg({
        title: _title,html:$(objData.id).html(),w:objData.w,h:objData.h,top:objData.top,
        success:function(){
          $(objData.id + "_label").html(objData.txt);
          $(objData.id + "_form :input[name='name']").bind('keyup',function(){
                comUtils.setPyAndWbCode(
                  $(this),
                  $(objData.id + "_form :input[name='py_code']"),
                  $(objData.id + "_form :input[name='wb_code']")
                );
          });
          $(objData.id + "_form :input[name='name']").focus();
          if(!$.isEmptyObject(jsonData))
          {
            _Index.setFormJson(objData.id + '_form',jsonData); 
          }
          layform.render();
        }
    },function(lo){
      var param = _Index.getFormJson(objData.id + '_form');
      param.dicttype = _dictType;
      _Index.ajax($(objData.id+"_form").attr('action'),param,function(d){
        if(d.err == 0)
        {
            $(objData.id+"_form :input[name='name']").val('');
            layer.close(lo);
            layer.msg(d.msg, {time: 1000},function(){
                _p = 1;
                _loadList();
            });
        }else{
            layer.msg(d.msg, {time: 2000,anim: 4});
        }
      },'');
    });
}
//添加事件
function _addAction(jsonData)
{
    jsonData = jsonData || '';
    if(_addType == 'no')
    {
        //什么也不干
    }else if(_addType == 'dialog'){
        var _w = 500, _h = 430, _txt = '名称', _top = 150;
        //职位
        if(_dictType == 'zhiwei')
        {
            _publicSetDialog({txt:'<i class="c-red">*</i>职位名称',id:'#tpl_zhiwei',w:_w,h:_h,top:_top,json:jsonData});
        }
        //支付方式
        if(_dictType == 'zffs')
        {
            _publicSetDialog({txt:'<i class="c-red">*</i>方式名称',id:'#tpl_zffs',w:_w,h:_h,top:_top,json:jsonData});
        }
        //费别明细
        if(_dictType == 'fbmx')
        {
            _publicSetDialog({txt:'<i class="c-red">*</i>费别名称',id:'#tpl_fbmx',w:_w,h:_h,top:_top,json:jsonData});
        }
        //患者来源
        if(_dictType == 'hztag' || _dictType == 'huanzhely' || _dictType == 'zxlx' || _dictType == 'zlxmunit' || _dictType == 'zlxmcate' || _dictType == 'drugcate' || _dictType == 'jcbwei')
        {
            _h = 370;
            _top = 160;
            if(_dictType == 'hztag') _txt = '<i class="c-red">*</i>标签名称';
            if(_dictType == 'huanzhely') _txt = '<i class="c-red">*</i>来源名称';
            if(_dictType == 'zxlx') _txt = '<i class="c-red">*</i>类型名称';
            if(_dictType == 'zlxmunit') _txt = '<i class="c-red">*</i>单位名称';
            if(_dictType == 'zlxmcate' || _dictType == 'drugcate') _txt = '<i class="c-red">*</i>分类名称';
            if(_dictType == 'jcbwei') _txt = '<i class="c-red">*</i>部位名称';
            _publicSetDialog({txt:_txt,id:'#tpl_dictsheet',w:_w,h:_h,top:_top,json:jsonData});
        }
    }else{
        window.location.href = _addType;
    }
}
//不同类型字典表格数据
function getTableConfigField()
{
    if(_dictType == 'zhiwei')
    {
      return [[
        {field:'name', minwidth:200, title: '职位名称'}
        ,{field:'is_ywry', width:120, align:'center', title: '是否医务人员'}
        ,{field:'py_code', width:220, align:'center', title: '拼音助记词', sort: true}
        ,{field:'wb_code', width:220, align:'center', title: '五笔助记词', sort: true}
        ,{field:'sort', width:150, align:'center',title: '显示顺序', sort: true}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center', sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    if(_dictType == 'zffs')
    {
      return [[
        {field:'other_val', width:220, title: '支付方式编号'}
        ,{field:'name', minwidth:200, title: '支付方式名称'}
        ,{field:'py_code', width:220, align:'center', title: '拼音助记词', sort: true}
        ,{field:'wb_code', width:220, align:'center', title: '五笔助记词', sort: true}
        ,{field:'sort', width:150, align:'center',title: '显示顺序', sort: true}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center', sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    if(_dictType == 'fbmx')
    {
      return [[
        {field:'name', minwidth:200, title: '费别明细名称'}
        ,{field:'other_val_txt', width:220, title: '费别类型'}
        ,{field:'py_code', width:220, align:'center', title: '拼音助记词', sort: true}
        ,{field:'wb_code', width:220, align:'center', title: '五笔助记词', sort: true}
        ,{field:'sort', width:150, align:'center',title: '显示顺序', sort: true}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center', sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    //患者来源
    if(_dictType == 'hztag' || _dictType == 'huanzhely' || _dictType == 'zxlx' || _dictType == 'zlxmunit' || _dictType == 'zlxmcate' || _dictType == 'drugcate' || _dictType == 'jcbwei')
    {
        var _txt = '名称';
        if(_dictType == 'hztag') _txt = '患者标签名称';
        if(_dictType == 'huanzhely') _txt = '患者来源名称';
        if(_dictType == 'zxlx') _txt = '咨询类型名称';
        if(_dictType == 'zlxmunit') _txt = '诊疗项目单位名称';
        if(_dictType == 'zlxmcate') _txt = '诊疗项目分类名称';
        if(_dictType == 'drugcate') _txt = '药品分类名称';
        if(_dictType == 'jcbwei') _txt = '检查部位名称';
        return [[
            {field:'name', minwidth:200, title: _txt}
            ,{field:'py_code', width:220, align:'center', title: '拼音助记词', sort: true}
            ,{field:'wb_code', width:220, align:'center', title: '五笔助记词', sort: true}
            ,{field:'sort', width:150, align:'center',title: '显示顺序', sort: true}
            ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
            ,{field:'create_time', width:180, title: '添加时间',align:'center', sort: true}
            ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
        ]];
    }
    return [];
}



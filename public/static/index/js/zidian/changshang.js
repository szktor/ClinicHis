$(function(){
  
});
//共用设置
function _publicSetDialog(objData)
{
    var jsonData = objData.json || '',_title = _dictText;
    if($.isEmptyObject(jsonData))
    {
       _title = _title + ' 添加';
    }else{
       _title = _title + ' 修改';
    }
    objData.txt = objData.txt || '输入名称';
    objData.top = objData.top || 0;
    _Index.tplDlg({
        title: _title,html:$(objData.id).html(),w:objData.w,h:objData.h,top:objData.top,
        success:function(){
          $(objData.id + "_label").html(objData.txt);
          $(objData.id + "_form :input[name='name']").bind('keyup',function(){
                comUtils.setPyAndWbCode(
                  $(this),
                  $(objData.id + "_form :input[name='py_code']"),
                  $(objData.id + "_form :input[name='wb_code']")
                );
          });
          $(objData.id + "_form :input[name='name']").focus();
          if(!$.isEmptyObject(jsonData))
          {
            _Index.setFormJson(objData.id + '_form',jsonData);
            //省份城市初始化
            initProvinceSelect(objData.id,jsonData.country,jsonData.province,jsonData.city);
          }else{
            $(objData.id + "_form :input[name='cs_type']").val(_dictType);
            //省份城市初始化
            initProvinceSelect(objData.id);
          }
          layform.render();
        }
    },function(lo){
      var param = _Index.getFormJson(objData.id + '_form');
      param.dicttype = _dictType;
      _Index.ajax($(objData.id+"_form").attr('action'),param,function(d){
        if(d.err == 0)
        {
            $(objData.id+"_form :input[name='name']").val('');
            layer.close(lo);
            layer.msg(d.msg, {time: 1000},function(){
                _p = 1;
                _loadList();
            });
        }else{
            layer.msg(d.msg, {time: 2000,anim: 4});
        }
      },'');
    });
}
//添加事件
function _addAction(jsonData)
{
    jsonData = jsonData || '';
    var _w = 1000, _h = 640, _txt = '名称', _top = 50;
    _publicSetDialog({txt:'<i class="c-red">*</i>厂商名称',id:'#tpl_csgl',w:_w,h:_h,top:_top,json:jsonData});
}
//表格数据
function getTableConfigField()
{
    return [[
        {field:'bianma', width:120, title: '编号'}
        ,{field:'name', minwidth:200, title: '名称'}
        ,{field:'cp_type_txt', width:100, align:'center', title: '产品类型'}
        ,{field:'tel', width:150, align:'center', title: '电话'}
        ,{field:'address_text', width:300,title: '地址'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, align:'center', title: '添加时间',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
    ]];
}
//省份城市初始化
function initProvinceSelect(obj,txtCountry,txtProvince,txtCity)
{
    txtCountry = txtCountry || '国内';
    txtProvince = txtProvince || '';
    txtCity = txtCity || '';
    //选择国家
    layform.on('select(country)', function(data) {
        //初始国家
        countryInit(obj,data.value,txtProvince,txtCity);
    });
    //初始国家
    countryInit(obj,txtCountry,txtProvince,txtCity);
}
//初始国家
function countryInit(obj,txtCountry,txtProvince,txtCity) {
    var province = $(obj + "_form :input[name='province']");
    if(txtCountry == '国外')
    {
        removeEle($(obj + "_form :input[name='province']"));
        removeEle($(obj + "_form :input[name='city']"));
    }else{
        //初始将省份数据赋予
        for (var i = 0; i < provinceList.length; i++) {
            addEle(province, provinceList[i].name,txtProvince);
        }
        removeEle($(obj + "_form :input[name='city']"));
        if(txtCity != '')
        {
            setCityOption(obj,txtProvince,txtCity); //设置省份对应的城市
        }else{
            setCityOption(obj,provinceList[0].name,txtCity); //设置省份对应的城市
        }
        //选定省份后 将该省份的数据读取追加上    
        layform.on('select(province)', function(data) {
            setCityOption(obj,data.value,''); //设置省份对应的城市
        });
    }
    layform.render('select');
}
//设置省份对应的城市
function setCityOption(obj,txtProvince,txtCity)
{
    txtProvince = txtProvince || '';
    txtCity = txtCity || '';
    var city = $(obj + "_form :input[name='city']");
    $.each(provinceList, function(i, item) {
        if (txtProvince == item.name) {
            cityItem = i;
            return cityItem;
        }
    });
    removeEle(city);
    $.each(provinceList[cityItem].cityList, function(i, item) {
        addEle(city, item.name, txtCity);
    });
    layform.render('select');
}
//向select中 追加内容
function addEle(ele, value, oldVal) {
    oldVal = oldVal || '';
    var optionStr = '', selectedStr = '';
    if(oldVal == value) selectedStr = ' selected="true" ';
    optionStr = "<option value=" + value + " "+selectedStr+">" + value + "</option>";
    ele.append(optionStr);
}
//移除select中所有项
function removeEle(ele) {
    ele.find("option").remove();
    var optionStar = "<option value=''>" + "请选择" + "</option>";
    ele.append(optionStar);
}
$(function(){
  
});
//共用设置
function _publicSetDialog(objData)
{
    var jsonData = objData.json || '',_title = _dictText;
    if($.isEmptyObject(jsonData))
    {
       _title = _title + ' 添加';
    }else{
       _title = _title + ' 修改';
    }
    objData.txt = objData.txt || '输入名称';
    objData.top = objData.top || 0;
    _Index.tplDlg({
        title: _title,html:$(objData.id).html(),w:objData.w,h:objData.h,top:objData.top,
        success:function(){
          $(objData.id + "_label").html(objData.txt);
          //$(objData.id + "_form :input").focus(function(){$(this).select();});
          $(objData.id + "_form :input[name='name']").bind('keyup',function(){
                comUtils.setPyAndWbCode(
                   $(this),
                   $(objData.id + "_form :input[name='py_code']"),
                   $(objData.id + "_form :input[name='wb_code']")
                );
          });
          $(objData.id + "_form :input[name='name']").focus();
          if(!$.isEmptyObject(jsonData))
          {
            _Index.setFormJson(objData.id + '_form',jsonData);
            if(_dictType == 'xy'){
                $('#findDrugDivBox').hide();
                setSelectUnitText(objData.id); //设置单位文本文字
                //计算拆零的单价
                $(objData.id + "_form :input[name='price']").bind('keyup',function(){
                    setChaiLingOnePrice(objData.id);
                });
                $(objData.id + "_form :input[name='zj_num']").bind('keyup',function(){
                    setChaiLingOnePrice(objData.id);
                });
            }
            if(_dictType == 'zy'){
                setSelectZyUnitText(objData.id); //设置--中药--单位文本文字
            }
            if(_dictType == 'cl'){
                setSelectClUnitText(objData.id); //设置--材料--单位文本文字
            } 
          }else{
            if(_dictType == 'xy')
            {
                setSelectUnitText(objData.id); //设置单位文本文字
                //计算拆零的单价
                $(objData.id + "_form :input[name='price']").bind('keyup',function(){
                    setChaiLingOnePrice(objData.id);
                });
                $(objData.id + "_form :input[name='zj_num']").bind('keyup',function(){
                    setChaiLingOnePrice(objData.id);
                });
                $(objData.id + "_form :input[name='cl_price']").bind('focus',function(){
                    setChaiLingOnePrice(objData.id);
                });
                //西药搜索
                $.myEasySearch({
                    selector:"#findSysDrugs", //#inputKeyWord
                    url:_findSysUrl,
                    height:200,
                    header:[
                        {title:"国药准字",field:"guoyaozhunzi",width:120},
                        {title:"名称",field:"name",width:250},
                        {title:"规格",field:"spec",width:120},
                        {title:"厂家",field:"changjia",width:290},
                    ],
                    click:function(d,o){
                        _Index.setFormJson(objData.id + '_form',d);
                        setSelectUnitText(objData.id);//设置单位文本文字
                        $(objData.id + "_form :input[name='price']").focus();
                        layform.render();
                    }
                });
            }
            if(_dictType == 'zy'){
                setSelectZyUnitText(objData.id); //设置--中药--单位文本文字
            }
            if(_dictType == 'cl'){
                setSelectClUnitText(objData.id); //设置--材料--单位文本文字
            }
          }
          layform.render();
          //西药单位选择绑定
          if(_dictType == 'xy')
          {
                layform.on('select(zj_unit)', function(_tv){
                    var _txt = _tv.elem[_tv.elem.selectedIndex].text;
                    $('#xy_form_cl_unit').html('元/'+_txt);
                });
                layform.on('select(kc_unit)', function(_tv){
                    var _txt = _tv.elem[_tv.elem.selectedIndex].text;
                    $('#xy_form_zj_unit').html('元/'+_txt);
                    $('#xy_form_ls_unit').html('元/'+_txt);
                });
          }
          //中药单位选择绑定
          if(_dictType == 'zy')
          {
                layform.on('select(zy_ls_unit)', function(_tv){
                    var _txt = _tv.elem[_tv.elem.selectedIndex].text;
                    $('#zy_form_ls_unit').html('元/'+_txt);
                    $('#zy_form_xj_unit').html('元/'+_txt);
                });
          }
          //材料单位选择绑定
          if(_dictType == 'cl')
          {
                layform.on('select(cl_ls_unit)', function(_tv){
                    var _txt = _tv.elem[_tv.elem.selectedIndex].text;
                    $(objData.id + "_form").children().find("select[name='kc_unit']").val(_tv.value);
                    $('#cl_form_ls_unit').html('元/'+_txt);
                    $('#cl_form_xj_unit').html('元/'+_txt);
                    layform.render('select');
                });
          }
        }
    },function(lo){
      var param = _Index.getFormJson(objData.id + '_form');
      param.dicttype = _dictType;
      _Index.ajax($(objData.id+"_form").attr('action'),param,function(d){
        if(d.err == 0)
        {
            $(objData.id+"_form :input[name='name']").val('');
            layer.close(lo);
            layer.msg(d.msg, {time: 1000},function(){
                _p = 1;
                _loadList();
            });
        }else{
            layer.msg(d.msg, {time: 2000,anim: 4});
        }
      },'');
    });
}
//添加事件
function _addAction(jsonData)
{
    jsonData = jsonData || '';
    var _w = 1000, _h = 740, _txt = '名称', _top = 50;
    //西药
    if(_dictType == 'xy')
    {
        if(!$.isEmptyObject(jsonData))
        {
            _h = 680;
        }
        _publicSetDialog({txt:'<i class="c-red">*</i>药品名称',id:'#tpl_xy',w:_w,h:_h,top:_top,json:jsonData});
    }
    //中药
    if(_dictType == 'zy')
    {
        _h = 620;
        _publicSetDialog({txt:'<i class="c-red">*</i>药品名称',id:'#tpl_zy',w:_w,h:_h,top:_top,json:jsonData});
    }
    //材料
    if(_dictType == 'cl')
    {
        _h = 600;
        _publicSetDialog({txt:'<i class="c-red">*</i>材料名称',id:'#tpl_cl',w:_w,h:_h,top:_top,json:jsonData});
    }
}
//不同类型字典表格数据
function getTableConfigField()
{
    if(_dictType == 'xy')
    {
      return [[
        {field:'name', minwidth:200, title: '名称'}
        ,{field:'cf_spec', width:120, align:'center', title: '处方规格',sort: true}
        ,{field:'stock', width:100, align:'center', title: '库存',sort: true}
        ,{field:'changjia', width:300, title: '厂家'}
        ,{field:'cl_price_txt', width:150, align:'center', title: '拆零价',sort: true}
        ,{field:'zj_unit_txt', width:100, align:'center', title: '拆零单位'}
        ,{field:'price', width:150, align:'center', title: '零售价',sort: true}
        ,{field:'ls_unit_txt', width:100, align:'center', title: '零售单位'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    if(_dictType == 'zy')
    {
      return [[
        {field:'name', minwidth:200, title: '名称'}
        ,{field:'stock', width:100, align:'center', title: '库存',sort: true}
        ,{field:'spec', width:120, align:'center', title: '规格'}
        ,{field:'changjia', width:300,title: '厂家'}
        ,{field:'yp_type', width:150, align:'center', title: '药品类型'}
        ,{field:'price', width:150, align:'center', title: '零售价',sort: true}
        ,{field:'ls_unit_txt', width:150, align:'center', title: '零售单位'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    if(_dictType == 'cl')
    {
      return [[
        {field:'name', minwidth:200, title: '名称'}
        ,{field:'stock', width:100, align:'center', title: '库存',sort: true}
        ,{field:'spec', width:120, align:'center', title: '规格'}
        ,{field:'cate_id_txt', minwidth:200, title: '分类'}
        ,{field:'changjia', width:300,title: '厂家'}
        ,{field:'price', width:150, align:'center', title: '零售价',sort: true}
        ,{field:'ls_unit_txt', width:150, align:'center', title: '零售单位'}
        ,{field:'kc_unit_txt', width:150, align:'center', title: '库存单位'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',align:'center',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
    }
    return [];
}

//设置--西药--单位文本文字 setSelectUnitText(objData.id)
function setSelectUnitText(_obj)
{
    var t1 = $(_obj + "_form").children().find("select[name='zj_unit']").find("option:selected").text();
    var t2 = $(_obj + "_form").children().find("select[name='kc_unit']").find("option:selected").text();
    if(t1.length > 0)
    {
        $('#xy_form_cl_unit').html('元/'+t1);
    }
    if(t2.length > 0)
    {
        $('#xy_form_ls_unit').html('元/'+t2);
        $('#xy_form_zj_unit').html('元/'+t2);
    }    
}
//设置--中药--单位文本文字 setSelectZyUnitText(objData.id)
function setSelectZyUnitText(_obj)
{

    var t1 = $(_obj + "_form").children().find("select[name='ls_unit']").find("option:selected").text();
    if(t1.length > 0)
    {
        $('#zy_form_ls_unit').html('元/'+t1);
        $('#zy_form_xj_unit').html('元/'+t1);
    }   
}
//设置--材料--单位文本文字 setSelectClUnitText(objData.id)
function setSelectClUnitText(_obj)
{
    var t1 = $(_obj + "_form").children().find("select[name='ls_unit']").find("option:selected").text();
    if(t1.length > 0)
    {
        $('#cl_form_ls_unit').html('元/'+t1);
        $('#cl_form_xj_unit').html('元/'+t1);
    }   
}
//计算拆零的单价
function setChaiLingOnePrice(_obj)
{
    var _dj = 0; //拆零的单价
    var _price = $(_obj + "_form :input[name='price']").val(); //零售价
    var _zj_num = $(_obj + "_form :input[name='zj_num']").val(); //制剂数量
    _dj = parseFloat(parseFloat(_price)/parseInt(_zj_num));
    if(isNaN(_dj))
    {
        $(_obj + "_form :input[name='cl_price']").val(''); //拆零价
    }else{
        $(_obj + "_form :input[name='cl_price']").val(parseFloat(_dj).toFixed(2)); //拆零价
    }
}
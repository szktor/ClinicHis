$(function(){
  
});
//不同类型字典表格数据
function getTableConfigField()
{
      return [[
        {field:'bianma', width:180, title: '组合编号'}
        ,{field:'name', minwidth:250, title: '组合名称'}
        ,{field:'price', width:120, align:'center', title: '售价',sort: true}
        ,{field:'ls_unit_txt', width:120, align:'center', title: '售价单位'}
        ,{field:'biaoben', width:180, align:'center', title: '标本/部位'}
        ,{field:'address_txt', width:180, align:'center', title: '地点'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:180, title: '添加时间',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
}
//跳转到新的连接
function goToNewPage(_url)
{
  __showLoadingBox('请稍候...');
  window.parent.setNewUrl(_url);
}
//设置-列表
function setTableTbodyHtml(d)
{
  var _idKeyName = d.type + '_' + d.id;
  if($('#tr_'+_idKeyName).length > 0)
  {
    layer.alert('【' + d.name + '】已在子项目列表中，请勿重复添加！', {title:'错误',anim: 6},function(i){
            layer.close(i);
    });
    return false;
  }
  var _html = '<tr data-type="'+d.type+'" data-id="'+d.id+'" data-price="'+d.price+'" data-json=\''+JSON.stringify(d)+'\' id="tr_'+_idKeyName+'">\
    <td>'+d.type_txt+'</td>\
    <td>'+d.bianma+'</td>\
    <td>'+d.name+'</td>\
    <td>'+d.price+'</td>\
    <td><input type="text" id="tr_ipt_'+_idKeyName+'" autocomplete="off" class="layui-input" value="1" style="height: 28px;"></td>\
    <td><a class="layui-btn layui-btn-danger layui-btn-xs" onclick="delTr(\'#tr_'+_idKeyName+'\');">删除</a></td>\
  </tr>';
  $('#subListBox').append(_html);
  $('#tr_ipt_'+_idKeyName).focus();
  $('#tr_ipt_'+_idKeyName).bind("keyup",function(){
      $(this).val($(this).val().replace(/[^\-?\d]/g,1));
      if(parseInt($(this).val()) <= 0)
      {
        $(this).val(1);
        layer.msg('请输入整数数值', {time: 1000}); 
      }
      reloadListPrice();
  });
  $('#tr_ipt_'+_idKeyName).focus(function(){$(this).select();}); 
  reloadListPrice();
}
//获取列表--数据
function getListData()
{
    var _list = {}, t = 0;
    if(parseInt($('#subListBox tr').length) <= 0) return _list;
    $('#subListBox tr').each(function(i,v){
        var _type = $(this).data('type'), _id = $(this).data('id'), _onePrice = $(this).data('price');
        var _idKeyName = _type + '_' + _id;
        _num = parseInt($('#tr_ipt_'+_idKeyName).val());
        if(_num > 0)
        {
            var o = $(this).data('json');
            o.num = _num;
            _list[t] = o;
            t++;
        }
    });
    return _list;
}
//删除-列表
function delTr(_obj)
{
  $(_obj).remove();
  reloadListPrice();
}
//重新计算价格
function reloadListPrice()
{
  var _totalPrice = parseFloat(0);
  if($('#subListBox tr').length > 0)
  {
    $('#subListBox tr').each(function(i,v){
        var _type = $(this).data('type'), _id = $(this).data('id'), _onePrice = $(this).data('price');
        var _idKeyName = _type + '_' + _id;
        _num = parseInt($('#tr_ipt_'+_idKeyName).val());
        if(_num > 0)
        {
            _totalPrice = parseFloat(parseFloat(_totalPrice)+parseFloat(_num*parseFloat(_onePrice)));
        }
    });
  }
  $("#info-form :input[name='zh_price']").val(parseFloat(_totalPrice).toFixed(2));
  if(_oldId == '') $("#info-form :input[name='price']").val(parseFloat(_totalPrice).toFixed(2));
}
//设置--文字
function setInfoTiChengUnitText(_obj)
{
    var t1 = $(_obj).children().find("select[name='xm_tc_mode']").find("option:selected").val();
    if(t1 == '1')
    {
        $('#zhenliao_form_tc_unit').html('%');
    }else{
        $('#zhenliao_form_tc_unit').html('');
    } 
}
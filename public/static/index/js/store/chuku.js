$(function(){
  
});
//不同类型字典表格数据
function getTableConfigField()
{
      return [[
        {field:'entry_sn', width:150, title: '出库单号',sort: true}
        ,{field:'create_time', width:120,align:'center', title: '出库日期',sort: true}
        ,{field:'entry_sub_type_txt', width:120,align:'center', title: '出库方式'}
        ,{field:'out_keshi', width:120, align:'center', title: '领用科室'}
        ,{field:'out_renyuan', width:120, align:'center', title: '领用人员'}
        ,{field:'add_name', width:120,align:'center',title: '制单人'}
        ,{field:'confim_name', width:120,align:'center',title: '确认人'}
        ,{field:'total_price', width:180,align:'center',title: '出库总金额（元）', sort: true}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'remarks', minwidth:250, title: '备注'}
        ,{fixed: 'right', width: 220, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
}
//跳转到新的连接
function goToNewPage(_url)
{
  __showLoadingBox('请稍候...');
  window.parent.setNewUrl(_url);
}
//设置-列表
function setTableTbodyHtml(d)
{
  var _idKeyName = d.id;
  if($('#tr_'+_idKeyName).length > 0)
  {
    layer.alert('【' + d.name + '】已在子项目列表中，请勿重复添加！', {title:'错误',anim: 6},function(i){
        layer.close(i);
    });
    return false;
  }
  var _html = '<tr data-id="'+d.id+'" data-num="'+d.num+'" data-price="'+d.price+'" data-json=\''+JSON.stringify(d)+'\' id="tr_'+_idKeyName+'">\
    <td>'+d.name+'</td>\
    <td>'+d.spec+'</td>\
    <td><input type="text" id="tr_iptnum_'+_idKeyName+'" autocomplete="off" class="layui-input" value="1" style="height: 28px;"></td>\
    <td>'+d.num+'</td>\
    <td>'+d.unit+'</td>\
    <td>'+d.ls_price+'/'+d.unit+'</td>\
    <td>'+d.price+'/'+d.unit+'</td>\
    <td>'+d.batch_sn+'</td>\
    <td>'+d.except_time+'</td>\
    <td><a class="layui-btn layui-btn-danger layui-btn-xs" onclick="delTr(\'#tr_'+_idKeyName+'\');">删除</a></td>\
  </tr>';
  $('#subListBox').append(_html);
  //初始化列表
  _initTableList(_idKeyName,true);
}
//初始化列表
function _initTableList(_idKeyName,_reload)
{
  _reload = _reload || false;
  $('#tr_iptnum_'+_idKeyName).focus();
  $('#tr_iptnum_'+_idKeyName).select();
  //input 只能输入整数 - 不能超最最大值
  _Index.bindInputNumTwo('#tr_iptnum_'+_idKeyName,$('#tr_'+_idKeyName).data('num'),function(){
      reloadListPrice();
  });
  if(_reload) reloadListPrice();
}
//获取列表--数据
function getListData()
{
    var _list = {}, t = 0;
    if(parseInt($('#subListBox tr').length) <= 0) return _list;
    $('#subListBox tr').each(function(i,v){
        var _id = parseInt($(this).data('id'));
        var _idKeyName = _id;
        var _oldNum = parseInt($(this).data('num')); //原有数量
        var _num = parseInt($('#tr_iptnum_'+_idKeyName).val()); //出库数量
        if(_num > 0)
        {
            if(_num > _oldNum)
            {
                layer.msg('数量输入有误，不可大于现有库存数量', {time: 2000,anim: 4});
                return _list;
            }
            var o = $(this).data('json');
            _list[t] = {};
            _list[t]['id'] = _id;
            _list[t]['json'] = o;
            _list[t]['num'] = _num;
            _list[t]['unit'] = o.unit; //单位
            _list[t]['price'] = parseFloat(o.price); //进货单价
            _list[t]['ls_price'] = o.ls_price; //零售单价
            _list[t]['batch_sn'] = o.batch_sn; //出库批号
            _list[t]['changjia'] = o.changjia; //厂家生产厂商
            _list[t]['chandi'] = o.chandi; //产地
            _list[t]['except_time'] = o.except_time; //有效期
            _list[t]['product_time'] = o.product_time; //生产日期
            t++;
        }
    });
    return _list;
}
//删除-列表
function delTr(_obj)
{
  $(_obj).remove();
  reloadListPrice();
}
//重新计算价格
function reloadListPrice()
{
  var _totalPrice = parseFloat(0); //出库总金额
  if($('#subListBox tr').length > 0)
  {
    $('#subListBox tr').each(function(i,v){
        var _id = parseInt($(this).data('id')), _oldNum = parseInt($(this).data('num')), _onePrice = parseFloat($(this).data('price')); //单价
        var _idKeyName = _id;
        //出库数量
        var _num = parseInt($('#tr_iptnum_'+_idKeyName).val());
        if(_num <= 0)
        {
          _num = 1;
          $('#tr_iptnum_'+_idKeyName).val('1');
        }
        if(_num > _oldNum)
        {
          _num = _oldNum;
          $('#tr_iptnum_'+_idKeyName).val(_oldNum);
        }
        _oneTotalPrice = parseFloat(_num*parseFloat(_onePrice));
        //计算 出库总金额
        _totalPrice = parseFloat(parseFloat(_totalPrice)+parseFloat(_oneTotalPrice));
    });
  }
  $("#info-form :input[name='total_price']").val(parseFloat(_totalPrice).toFixed(2));
}
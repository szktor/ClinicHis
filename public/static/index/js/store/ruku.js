$(function(){
  
});
//不同类型字典表格数据
function getTableConfigField()
{
      return [[
        {field:'entry_sn', width:150, title: '入库单号',sort: true}
        ,{field:'create_time', width:120,align:'center', title: '入库日期',sort: true}
        ,{field:'entry_sub_type_txt', width:120,align:'center', title: '入库方式'}
        ,{field:'changjia', width:200, title: '供应商'}
        ,{field:'add_name', width:120,align:'center',title: '制单人'}
        ,{field:'confim_name', width:120,align:'center',title: '确认人'}
        ,{field:'total_price', width:180,align:'center',title: '入库总金额（元）', sort: true}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'remarks', minwidth:250, title: '备注'}
        ,{fixed: 'right', width: 220, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
}
//跳转到新的连接
function goToNewPage(_url)
{
  __showLoadingBox('请稍候...');
  window.parent.setNewUrl(_url);
}
//设置-列表
function setTableTbodyHtml(d)
{
  var _idKeyName = d.id;
  if($('#tr_'+_idKeyName).length > 0)
  {
    layer.alert('【' + d.name + '】已在子项目列表中，请勿重复添加！', {title:'错误',anim: 6},function(i){
        layer.close(i);
    });
    return false;
  }
  var _html = '<tr data-id="'+d.id+'" data-price="'+d.price+'" data-json=\''+JSON.stringify(d)+'\' id="tr_'+_idKeyName+'">\
    <td>'+d.name+'</td>\
    <td>'+d.spec+'</td>\
    <td><input type="text" id="tr_iptnum_'+_idKeyName+'" autocomplete="off" class="layui-input" value="1" style="height: 28px;"></td>\
    <td>'+d.ls_unit+'</td>\
    <td style="display:flex;"><input type="text" id="tr_iptprice_'+_idKeyName+'" autocomplete="off" class="layui-input" value="0" style="height: 28px;width:65%;"><span style="margin:4px 0px 0px 3px;font-size:12px;">/'+d.ls_unit+'</span></td>\
    <td><input type="text" id="tr_ipttotal_'+_idKeyName+'" autocomplete="off" class="layui-input" value="0.00" style="height: 28px;" disabled="true"></td>\
    <td><input type="text" id="tr_iptbatch_'+_idKeyName+'" autocomplete="off" class="layui-input" value="" style="height: 28px;"></td>\
    <td>'+d.price+'/'+d.ls_unit+'</td>\
    <td><input type="text" id="tr_iptlstotal_'+_idKeyName+'" autocomplete="off" class="layui-input" value="0.00" style="height: 28px;" disabled="true"></td>\
    <td><input type="text" id="tr_iptexceptdate_'+_idKeyName+'" autocomplete="off" class="layui-input" value="" style="height: 28px;" readonly="true"></td>\
    <td><input type="text" id="tr_iptchangjia_'+_idKeyName+'" autocomplete="off" class="layui-input" value="'+d.changjia+'" style="height: 28px;"></td>\
    <td><input type="text" id="tr_iptproductdate_'+_idKeyName+'" autocomplete="off" class="layui-input" value="" style="height: 28px;" readonly="true"></td>\
    <td><input type="text" id="tr_iptchandi_'+_idKeyName+'" autocomplete="off" class="layui-input" value="'+d.chandi+'" style="height: 28px;"></td>\
    <td><a class="layui-btn layui-btn-danger layui-btn-xs" onclick="delTr(\'#tr_'+_idKeyName+'\');">删除</a></td>\
  </tr>';
  $('#subListBox').append(_html);
  //初始化列表
  _initTableList(_idKeyName,true);
}
//初始化列表
function _initTableList(_idKeyName,_reload)
{
  _reload = _reload || false;
  //日期
  laydate.render({ elem: '#tr_iptexceptdate_'+_idKeyName });
  laydate.render({ elem: '#tr_iptproductdate_'+_idKeyName });
  $('#tr_iptnum_'+_idKeyName).focus();
  $('#tr_iptnum_'+_idKeyName).select();
  //只能输入数字
  _Index.bindInputNum('#tr_iptnum_'+_idKeyName,1,function(){
      reloadListPrice();
  });
  //只能输入价格
  _Index.bindMoney('#tr_iptprice_'+_idKeyName,0,function(){
      reloadListPrice();
  });
  $('#tr_iptbatch_'+_idKeyName).focus(function(){$(this).select();});
  $('#tr_iptchangjia_'+_idKeyName).focus(function(){$(this).select();});
  $('#tr_iptchandi_'+_idKeyName).focus(function(){$(this).select();});
  if(_reload) reloadListPrice();
}
//获取列表--数据
function getListData()
{
    var _list = {}, t = 0;
    if(parseInt($('#subListBox tr').length) <= 0) return _list;
    $('#subListBox tr').each(function(i,v){
        var _id = parseInt($(this).data('id'));
        var _idKeyName = _id;
        var _num = parseInt($('#tr_iptnum_'+_idKeyName).val()); //出入库数量
        var _batchSn = $.trim($('#tr_iptbatch_'+_idKeyName).val()); //出入库批号
        if(_num > 0) //&& _batchSn != ''
        {
            var o = $(this).data('json');
            _list[t] = {};
            _list[t]['id'] = _id;
            _list[t]['json'] = o;
            _list[t]['num'] = _num;
            _list[t]['unit'] = o.ls_unit; //单位
            _list[t]['price'] = parseFloat($('#tr_iptprice_'+_idKeyName).val()); //进货单价
            _list[t]['total_price'] = parseFloat($('#tr_ipttotal_'+_idKeyName).val()); //进货总金额
            _list[t]['ls_price'] = o.price; //零售单价
            _list[t]['ls_total_price'] = parseFloat($('#tr_iptlstotal_'+_idKeyName).val()); //零售总金额
            _list[t]['batch_sn'] = _batchSn; //出入库批号
            _list[t]['changjia'] = $.trim($('#tr_iptchangjia_'+_idKeyName).val()); //厂家生产厂商
            _list[t]['chandi'] = $.trim($('#tr_iptchandi_'+_idKeyName).val()); //产地
            _list[t]['except_time'] = $.trim($('#tr_iptexceptdate_'+_idKeyName).val()); //有效期
            _list[t]['product_time'] = $.trim($('#tr_iptproductdate_'+_idKeyName).val()); //生产日期
            t++;
        }
    });
    return _list;
}
//删除-列表
function delTr(_obj)
{
  $(_obj).remove();
  reloadListPrice();
}
//重新计算价格
function reloadListPrice()
{
  var _totalPrice = parseFloat(0); //入库总金额
  var _oneTotalPrice = parseFloat(0); //单条进货总金额
  var _oneLsTotalPrice = parseFloat(0); //单条零售总金额
  if($('#subListBox tr').length > 0)
  {
    $('#subListBox tr').each(function(i,v){
        var _id = parseInt($(this).data('id')), _onePrice = parseFloat($(this).data('price')); //零售单价
        var _idKeyName = _id;
        //入库数量
        var _num = parseInt($('#tr_iptnum_'+_idKeyName).val());
        if(_num <= 0)
        {
          _num = 1;
          $('#tr_iptnum_'+_idKeyName).val('1');
        }
        //入库单价
        var _entryPrice = parseFloat($('#tr_iptprice_'+_idKeyName).val());
        if(_entryPrice <= 0)
        {
          _entryPrice = 0;
          $('#tr_iptprice_'+_idKeyName).val('0');
        }
        //1.计算 单条进货总金额
        _oneTotalPrice = parseFloat(_num*parseFloat(_entryPrice));
        //2.计算 单条零售总金额
        _oneLsTotalPrice = parseFloat(_num*parseFloat(_onePrice));
        //3.计算 入库总金额
        _totalPrice = parseFloat(parseFloat(_totalPrice)+parseFloat(_oneTotalPrice));
        //单条进货总金额 - 重新填写
        $('#tr_ipttotal_'+_idKeyName).val(parseFloat(_oneTotalPrice).toFixed(2));
        //单条零售总金额 - 重新填写
        $('#tr_iptlstotal_'+_idKeyName).val(parseFloat(_oneLsTotalPrice).toFixed(2));
    });
  }
  $("#info-form :input[name='total_price']").val(parseFloat(_totalPrice).toFixed(2));
}
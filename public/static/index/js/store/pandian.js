$(function(){
  
});
//不同类型字典表格数据
function getTableConfigField()
{
      return [[
        {field:'pandian_sn', width:180, title: '盘点单号',sort: true}
        ,{field:'create_time', width:150,align:'center', title: '盘点日期',sort: true}
        ,{field:'total_num', width:150,align:'center', title: '明细总数', sort: true}
        ,{field:'cost_price', width:200,align:'center',title: '进货盈亏合计', sort: true}
        ,{field:'ls_price', width:200,align:'center',title: '零售盈亏合计', sort: true}
        ,{field:'add_name', width:130,align:'center',title: '制单人'}
        ,{field:'confim_name', width:130,align:'center',title: '确认人'}
        ,{field:'status', width:120, title: '状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'remarks', minwidth:250, title: '备注'}
        ,{fixed: 'right', width: 220, align:'center', title: '操作', toolbar: '#table-content-com'}
      ]];
}
//跳转到新的连接
function goToNewPage(_url)
{
  __showLoadingBox('请稍候...');
  window.parent.setNewUrl(_url);
}
//设置-列表
function setTableTbodyHtml(_data)
{
    $('#subListBox').empty();
    $.each(_data.data,function(i,d){
      var _idKeyName = d.drugs_id;
      var _html = '<tr data-drugsid="'+d.drugs_id+'" data-stock="'+d.stock+'" data-costprice="'+d.cost_price+'" data-price="'+d.price+'" data-json=\''+JSON.stringify(d)+'\' id="tr_'+_idKeyName+'">\
          <td>'+d.name+'</td>\
          <td style="text-align:center;">'+d.spec+'</td>\
          <td style="text-align:center;">'+d.huogui_no+'</td>\
          <td>'+d.changjia+'</td>\
          <td style="text-align:center;">'+d.cost_price+'</td>\
          <td style="text-align:center;">'+d.price+'</td>\
          <td style="text-align:center;">'+d.unit+'</td>\
          <td style="text-align:center;">'+d.stock+'</td>\
          <td style="text-align:center;"><input type="text" id="tr_iptnum_'+_idKeyName+'" autocomplete="off" class="layui-input" value="'+d.stock_after+'" style="height: 28px;"></td>\
          <td style="text-align:center;" id="td_jhyk_'+_idKeyName+'">'+d.cost_price_total+'</td>\
          <td style="text-align:center;" id="td_lsyk_'+_idKeyName+'">'+d.price_total+'</td>\
          <td style="text-align:center;"><a class="layui-btn layui-btn-danger layui-btn-xs" onclick="delTr(\'#tr_'+_idKeyName+'\');">删除</a></td>\
      </tr>';
      $('#subListBox').append(_html);
      //只能输入数字
      setInputNum('#tr_iptnum_'+_idKeyName,d.stock,function(){
        reloadListPrice();
      });
      //重新计算价格-单条
      reloadOnetrPrice(_idKeyName);
    });
}
//input 只能输入整数
function setInputNum(obj,_val,_fun){
  $(obj).bind("keyup",function(){
      $(this).val($(this).val().replace(/[^\-?\d]/g,_val));
      var _v = parseInt($(this).val());
      if(_v < 0 || isNaN(_v))
      {
        $(this).val(_val);
        layer.msg('请输入整数数值', {time: 1000});
      }
      if(typeof(_fun) == 'function') _fun($(this).val());
  });
  $(obj).focus(function(){$(this).select();});
}
//获取列表--数据
function getListData()
{
    var _list = {}, t = 0;
    if(parseInt($('#subListBox tr').length) <= 0) return _list;
    $('#subListBox tr').each(function(i,v){
        var _idKeyName = parseInt($(this).data('drugsid'));
        var _num = parseInt($('#tr_iptnum_'+_idKeyName).val()); //实际盘点数量
        var o = $(this).data('json');
        _list[t] = {};
        _list[t]['drugs_id'] = _idKeyName;
        _list[t]['xm_type'] = o.xm_type;
        _list[t]['name'] = o.name;
        _list[t]['py_code'] = o.py_code;
        _list[t]['wb_code'] = o.wb_code;
        _list[t]['spec'] = o.spec;
        _list[t]['unit'] = o.unit;
        _list[t]['cost_price'] = o.cost_price;
        _list[t]['price'] = o.price;
        _list[t]['huogui_no'] = o.huogui_no;
        _list[t]['stock'] = o.stock;
        _list[t]['stock_after'] = _num;
        _list[t]['cost_price_total'] = parseFloat($('#td_jhyk_' + _idKeyName).html());
        _list[t]['price_total'] = parseFloat($('#td_lsyk_' + _idKeyName).html());
        _list[t]['changjia'] = o.changjia;
        _list[t]['remarks'] = '';
        t++;
    });
    return _list;
}
//删除-列表
function delTr(_obj)
{
  $(_obj).remove();
  reloadListPrice();
}
//重新计算价格
function reloadListPrice()
{
  _allPrice = parseFloat(0); //零售总金额
  _allCostPrice = parseFloat(0); //进货总金额
  var _oneTotalPrice = parseFloat(0); //单条零售总金额
  var _oneTotalCostPrice = parseFloat(0); //单条进货总金额
  if($('#subListBox tr').length > 0)
  {
      $('#subListBox tr').each(function(i,v){
          var _id = parseInt($(this).data('drugsid'));
          //重新计算价格-单条
          reloadOnetrPrice(_id);
      });
  }
  if(!isNaN(_allPrice) && !isNaN(_allCostPrice))
  {
      $('.line-top-tjbox').html('进货盈亏合计：' + parseFloat(_allCostPrice).toFixed(2) + '，零售盈亏合计：'+parseFloat(_allPrice).toFixed(2));
  }else{
      $('.line-top-tjbox').html('进货盈亏合计：0，零售盈亏合计：0');
  }
}
//重新计算价格-单条
function reloadOnetrPrice(_id)
{
    var _onePrice = parseFloat($('#tr_' + _id).data('price')); //零售单价
    var _oneCostPrice = parseFloat($('#tr_' + _id).data('costprice')); //进货单价
    var _oneStock = parseInt($('#tr_' + _id).data('stock')); //原有库存
    //盘点的数量
    var _num = parseInt($('#tr_iptnum_'+_id).val())-_oneStock;
    _oneTotalPrice = parseFloat(_num*_onePrice); //零售
    _oneTotalCostPrice = parseFloat(_num*_oneCostPrice); //进货
    if(!isNaN(_oneTotalPrice) && !isNaN(_oneTotalCostPrice))
    {
        $('#td_jhyk_' + _id).html(parseFloat(_oneTotalCostPrice).toFixed(2)); //进货
        $('#td_lsyk_' + _id).html(parseFloat(_oneTotalPrice).toFixed(2)); //零售
        _allPrice = parseFloat(parseFloat(_allPrice)+parseFloat(_oneTotalPrice));
        _allCostPrice = parseFloat(parseFloat(_allCostPrice)+parseFloat(_oneTotalCostPrice));
    }else{
        $('#td_jhyk_' + _id).html(parseFloat('0').toFixed(2)); //进货
        $('#td_lsyk_' + _id).html(parseFloat('0').toFixed(2)); //零售
    }
}
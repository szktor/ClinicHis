$(function(){
  
});
//共用设置
function _publicSetDialog(objData)
{
    var jsonData = objData.json || '',_title = '科室信息';
    if($.isEmptyObject(jsonData))
    {
       _title = _title + ' 添加';
    }else{
       _title = _title + ' 修改';
    }
    objData.txt = objData.txt || '科室名称';
    objData.top = objData.top || 0;
    _Index.tplDlg({
        title: _title,html:$(objData.id).html(),w:objData.w,h:objData.h,top:objData.top,
        success:function(){
            if(!$.isEmptyObject(jsonData))
            {
                _Index.setFormJson(objData.id + '_form',jsonData);
            }
            $(objData.id + "_form :input[name='name']").bind('keyup',function(){
                comUtils.setPyCode(
                   $(this),
                   $(objData.id + "_form :input[name='bianma']")
                );
            });
            $(objData.id + "_form :input[name='name']").focus();
            layform.render();
        }
    },function(lo){
      var param = _Index.getFormJson(objData.id + '_form');
      _Index.ajax($(objData.id+"_form").attr('action'),param,function(d){
        if(d.err == 0)
        {
            $(objData.id+"_form :input[name='name']").val('');
            layer.close(lo);
            layer.msg(d.msg, {time: 1000},function(){
                _p = 1;
                _loadList();
            });
        }else{
            layer.msg(d.msg, {time: 2000,anim: 4});
        }
      },'');
    });
}
//添加事件
function _addAction(jsonData)
{
    jsonData = jsonData || '';
    var _w = 680, _h = 420, _top = 150;
    _publicSetDialog({id:'#tpl_keshi',w:_w,h:_h,top:_top,json:jsonData});
}
//表格数据
function getTableConfigField()
{
    return [[
        {field:'bianma', width:120, title: '科室编号', sort: true}
        ,{field:'name', minwidth:200, title: '科室名称'}
        ,{field:'attr_txt', width:130, align:'center', title: '科室属性'}
        ,{field:'sort', width:130, align:'center', title: '显示顺序', sort: true}
        ,{field:'pid_txt', width:150, align:'center', title: '上级科室'}
        ,{field:'open_yuyue', width:150, align:'center', title: '开通预约',templet: "#yuyueTpl"}
        ,{field:'status', width:140, title: '科室状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:170, title: '添加时间',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
    ]];
}
$(function(){
  
});
//共用设置
function _publicSetDialog(objData)
{
    objData.top = objData.top || 0;
    _Index.tplDlg({
        title: '添加人员',html:$(objData.id).html(),w:objData.w,h:objData.h,top:objData.top,
        success:function(){
          _sTimer = null;
          _t = 60;
          $(objData.id + "_form :input[name='nickname']").focus();
          _loadSysAccount(objData.id + "_form :input[name='user_name']");
          //点击刷新验证码
          $('.captcha-div img').click(function(){ _setNewCaptcha(); });
          //设置验证码
          _setNewCaptcha();
          //点击发送验证码
          $('.tel-codebtn').click(function(){ _setSendCode(); });
          layform.render();
        }
    },function(lo){
      var param = _Index.getFormJson(objData.id + '_form');
      param.captcha_key = _captchaKey;
      _Index.ajax($(objData.id+"_form").attr('action'),param,function(d){
        _sTimer = null;
        _t = 60;
        if(d.err == 0)
        {
            $(objData.id+"_form :input[name='nickname']").val('');
            layer.close(lo);
            layer.msg(d.msg, {time: 1000},function(){
                _p = 1;
                _loadList();
            });
        }else{
            layer.msg(d.msg, {time: 2000,anim: 4});
        }
      },'');
    });
}
//添加事件
function _addAction()
{
    var _w = 680, _h = 450, _top = 150;
    _publicSetDialog({id:'#tpl_addadmin',w:_w,h:_h,top:_top});
}
//表格数据
function getTableConfigField()
{
    return [[
        {type:'checkbox'}
        ,{field:'user_name', minwidth:200, title: '登录账号'}
        ,{field:'user_mobile', width:150, align:'center', title: '手机号码', sort: true}
        ,{field:'nickname', width:130, align:'center', title: '姓名'}
        ,{field:'sex_txt', width:80, align:'center', title: '性别'}
        ,{field:'zhiwei_code_txt', width:120, align:'center', title: '职位'}
        ,{field:'gonghao_code', width:100, align:'center', title: '工号'}
        ,{field:'group_name', width:120, align:'center', title: '角色'}
        ,{field:'login_number', width:160, align:'center',title: '已登录(次)', sort: true}
        ,{field:'last_login_time', width:180, align:'center',title: '最后登录'}
        ,{field:'status', width:140, title: '账号状态',align:'center',templet: "#statusTpl", sort: true}
        ,{field:'create_time', width:170, title: '添加时间',sort: true}
        ,{fixed: 'right', width: 150, align:'center', title: '操作', toolbar: '#table-content-com'}
    ]];
}
//加载系统账号
function _loadSysAccount(_obj)
{
  _Index.ajax(globalCfg.buildSystemAdminUrl,{},function(d){
    if(d.err == 0)
    {
      $(_obj).val(d.data);
      layer.msg(d.msg, {time: 1000},function(){ });
    }else{
      layer.msg(d.msg, {time: 2000,anim: 4});
    }
  },'请稍候...');
}